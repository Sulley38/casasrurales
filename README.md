# Rural Houses #

### By Iván Matellanes and Luis Javier Salvatierra ###

This project was developed on March-May 2013, as part of the subject 'Software Engineering I'. It is a booking management software for rural house owners and clients, written in Java.

The project emphasizes on client/server architecture divided in 3 layers: Presentation, Business Logic, and Data Access/Persistence.

The development was accomplished using Scrum, an iterative and incremental agile software development model.


# Casas Rurales #

### Por Iván Matellanes y Luis Javier Salvatierra ###

Este proyecto fue desarrollado entre marzo y mayo de 2013 como parte de la asignatura 'Ingeniería del Software I'. Es un software de gestión de reservas para clientes y propietarios de casas rurales, escrito en Java.

El proyecto hace énfasis en la arquitectura cliente/servidor dividida en 3 capas: Presentación, Lógica de negocio, y Acceso a datos/Persistencia.

El desarrollo se llevó a cabo aplicando la metodología Scrum, un modelo de desarrollo ágil iterativo e incremental.
