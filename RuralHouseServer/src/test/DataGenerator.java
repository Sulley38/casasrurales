package test;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Vector;

import configuration.Config;

import dataAccess.DB4oManager;
import domain.*;

public class DataGenerator {

	@SuppressWarnings("unused")
	public static void main(String[] args) throws ParseException {
		DB4oManager accesoDatos = new DB4oManager();
		DateFormat fecha = Config.getShortDateFormat();
		Client c;
		Owner o;
		
		accesoDatos.registrarCliente("ljsalvat@gmail.com", "luis".toCharArray(), "943012345");
		accesoDatos.registrarPropietario("ivanma34@gmail.com", "ivan".toCharArray(), "943123789", "21456147740398669311");
		if (accesoDatos.listadoUsuarios().get(0) instanceof Client) {
			c = (Client) accesoDatos.listadoUsuarios().get(0);
			o = (Owner) accesoDatos.listadoUsuarios().get(1);
		} else {
			c = (Client) accesoDatos.listadoUsuarios().get(1);
			o = (Owner) accesoDatos.listadoUsuarios().get(0);
		}
		// Crear casas (añadir imágenes a mano)
		RuralHouse r1 = accesoDatos.crearCasaRural("Situado en la ladera sur del Monte Igeldo con atractivas vistas sobre la ciudad de Donostia-San Sebastián de cuyo centro le separan apenas 5km", "Donostia", 5, 2, 3, 1, 3, new Vector<byte[]>(), o); // Ofertas pasadas, comentarios
		RuralHouse r2 = accesoDatos.crearCasaRural("Caserio del siglo XV, reformado y con encanto. A 10' de la playa de Hondarribia.", "Oiartzun", 4, 2, 2, 1, 6, new Vector<byte[]>(), o); // Ofertas pasadas y futuras, algún comentario
		RuralHouse r3 = accesoDatos.crearCasaRural("Casa tradicional de finales del siglo XVIII, recientemente reformada", "Donostia", 6, 2, 4, 2, 3, new Vector<byte[]>(), o); // Ofertas futuras, conflictos
		RuralHouse r4 = accesoDatos.crearCasaRural("Riesgo constante de asalto, hay tiroteos frecuentes en la zona", "Altza", 3, 1, 2, 1, 0, new Vector<byte[]>(), o); // Una oferta pasada, con comentario
		RuralHouse r5 = accesoDatos.crearCasaRural("Piedra y fuego se unen para crear un acogedor, cómodo y elegante entorno", "Azpeitia", 8, 2, 5, 2, 6, new Vector<byte[]>(), o); // Reservas pasadas y futuras
		RuralHouse r6 = accesoDatos.crearCasaRural("Las Cuevas Algarves de Gorafe están situadas en la parte alta del Barrio del Castillo, en una zona paisajística, serena y sin contaminación lumínica.", "Gorafe", 8, 2, 5, 2, 6, new Vector<byte[]>(), o); // Sin ofertas
		// Crear ofertas
		Offer o11 = accesoDatos.crearOferta(r1, fecha.parse("06/04/2013"), fecha.parse("09/04/2013"), 180.55f);
		Offer o12 = accesoDatos.crearOferta(r1, fecha.parse("15/04/2013"), fecha.parse("20/04/2013"), 220.30f);
		Offer o13 = accesoDatos.crearOferta(r1, fecha.parse("26/04/2013"), fecha.parse("28/04/2013"), 140.08f);
		Offer o14 = accesoDatos.crearOferta(r1, fecha.parse("02/05/2013"), fecha.parse("06/05/2013"), 190.00f);
		Offer o21 = accesoDatos.crearOferta(r2, fecha.parse("21/04/2013"), fecha.parse("24/04/2013"), 91.20f);
		Offer o22 = accesoDatos.crearOferta(r2, fecha.parse("30/04/2013"), fecha.parse("02/05/2013"), 75.70f);
		Offer o23 = accesoDatos.crearOferta(r2, fecha.parse("17/05/2013"), fecha.parse("20/05/2013"), 81.43f);
		Offer o24 = accesoDatos.crearOferta(r2, fecha.parse("01/06/2013"), fecha.parse("04/06/2013"), 92.00f);
		Offer o25 = accesoDatos.crearOferta(r2, fecha.parse("05/06/2013"), fecha.parse("11/06/2013"), 115.99f);
		Offer o26 = accesoDatos.crearOferta(r2, fecha.parse("21/06/2013"), fecha.parse("25/06/2013"), 128.00f);
		Offer o31 = accesoDatos.crearOferta(r3, fecha.parse("22/05/2013"), fecha.parse("24/05/2013"), 110.00f);
		Offer o32 = accesoDatos.crearOferta(r3, fecha.parse("23/05/2013"), fecha.parse("25/05/2013"), 120.00f);
		Offer o33 = accesoDatos.crearOferta(r3, fecha.parse("24/05/2013"), fecha.parse("26/05/2013"), 130.00f);
		Offer o34 = accesoDatos.crearOferta(r3, fecha.parse("29/05/2013"), fecha.parse("31/05/2013"), 115.00f);
		Offer o35 = accesoDatos.crearOferta(r3, fecha.parse("31/05/2013"), fecha.parse("03/06/2013"), 160.00f);
		Offer o41 = accesoDatos.crearOferta(r4, fecha.parse("04/05/2013"), fecha.parse("05/05/2013"), 20.00f);
		Offer o51 = accesoDatos.crearOferta(r5, fecha.parse("15/02/2013"), fecha.parse("17/02/2013"), 60.00f);
		Offer o52 = accesoDatos.crearOferta(r5, fecha.parse("20/03/2013"), fecha.parse("24/03/2013"), 81.00f);
		Offer o53 = accesoDatos.crearOferta(r5, fecha.parse("23/05/2013"), fecha.parse("25/05/2013"), 87.10f);
		Offer o54 = accesoDatos.crearOferta(r5, fecha.parse("29/05/2013"), fecha.parse("01/06/2013"), 99.61f);
		Offer o55 = accesoDatos.crearOferta(r5, fecha.parse("14/06/2013"), fecha.parse("16/06/2013"), 102.83f);
		// Hacer reservas
		Book b11 = accesoDatos.crearReserva(o11, null, Book.TOTAL_PAID);
		Book b12 = accesoDatos.crearReserva(o12, c, Book.TOTAL_PAID);
		Book b13 = accesoDatos.crearReserva(o13, null, Book.TOTAL_PAID);
		Book b14 = accesoDatos.crearReserva(o14, null, Book.TOTAL_PAID);
		Book b21 = accesoDatos.crearReserva(o21, null, Book.TOTAL_PAID);
		Book b22 = accesoDatos.crearReserva(o22, c, Book.TOTAL_PAID);
		Book b23 = accesoDatos.crearReserva(o23, c, Book.PAY_AT_CHECKIN);
		Book b41 = accesoDatos.crearReserva(o41, c, Book.TOTAL_PAID);
		Book b51 = accesoDatos.crearReserva(o51, c, Book.TOTAL_PAID);
		Book b53 = accesoDatos.crearReserva(o53, c, Book.DEPOSIT_PAID);
		Book b55 = accesoDatos.crearReserva(o55, c, Book.PENDING);
		// Comentarios
		accesoDatos.crearComentario(b11, "Fue sin duda la mejor casa rural en la que estuvimos, si la hubiera preparado a mi gusto no me quedaría tan bien. Los dueños son encantadores y muy serviciales.", 5.0f);
		accesoDatos.crearComentario(b12, "Es amplia pero es una casa antigua con pocas ventanas al exterior. Pero lo peor que nos encontramos fue la limpieza. La casa olía a cerrado y la cocina la verdad que estaba sucísima.", 2.5f);
		accesoDatos.crearComentario(b13, "Todo ha estado genial. El sitio perfecto. Paz y tranquilidad absoluta. Las cenas de una calidad excepcional.", 5.0f);
		accesoDatos.crearComentario(b14, "La casa es tal y como se refleja en la web y el propietario es una persona seria y atenta. Nos alojamos allí un grupo de amigos (6 adultos y 2 niños) durante 5 días. Nuestra estancia fue agradable y su ubicación nos permitió disfrutar de los atractivos de la zona.", 4.0f);
		accesoDatos.crearComentario(b22, "Estuvimos alojados mi novio y yo este pasado mes. Destacar la decoración de las habitaciones y la casa en general, la limpieza y el trato de Iñaki que nos dio un mapa y nos planifico rutas para hacer por la zona.", 4.5f);
		accesoDatos.crearComentario(b41, "Te encuentras con el problema de que es una casa en la que la luz funcina con un motor de gasolina, y el jefe le echa 2 litros de gasolina, cuya consecuencia es que a las 11 de la noche estas sin luz en la casa y cocinando con una linterna.", 0.5f);
		
		accesoDatos.cerrarBD();
	}

}
