package domain;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Clase de reserva
 */
public class Book implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public static final String[] statusText = {"No pagado", "Depósito pagado", "Pagar en mano", "Totalmente pagado"};
	public static final int PENDING = 0, DEPOSIT_PAID = 1, PAY_AT_CHECKIN = 2, TOTAL_PAID = 3;
	
	private final String bookCode;
	private Date bookDate;
	private int status;
	private boolean commentMade;
	private Client client;
	private Offer offer;
	
	/**
	 * Constructora para reserva anónima.
	 * El código es un identificador UUID aleatorio de 128 bits. Según Wikipedia, la probabilidad de que
	 * se generen dos códigos iguales es bastante menor que la probabilidad de que una persona muera por
	 * impacto de meteorito, así que no controlamos duplicados.
	 * @param offer - the offer that it is booked
	 */
	public Book(Offer offer) {
		this.bookCode = UUID.randomUUID().toString();
		this.bookDate = new Date();
		this.status = PENDING;
		this.commentMade = false;
		this.client = null;
		this.offer = offer;
	}
	
	/**
	 * Constructora para asociar con un cliente.
	 * @param client - the client that makes the booking, or null if anonymous
	 * @param offer - the offer that it is booked
	 */
	public Book(Client client, Offer offer) {
		this.bookCode = UUID.randomUUID().toString();
		this.bookDate = new Date();
		this.status = PENDING;
		this.commentMade = false;
		this.client = client;
		this.offer = offer;
		client.addBooking(this);
	}

	/**
	 * Get the book code
	 * @return book code
	 */
	public String getBookCode() {
		return this.bookCode;
	}
	
	/**
	 * This method sets a book date
	 * @param bookDate - new booking date
	 */
	public void setBookDate(Date bookDate) {
		this.bookDate = bookDate;
	}

	/**
	 * This method returns a book date
	 * @return book date
	 */
	public Date getBookDate() {
		return this.bookDate;
	}
	
	/**
	 * This method returns the status of a book 
	 * @return status code
	 */
	public int getStatus() {
		return status;
	}
	
	/**
	 * This method sets the booking status
	 * @param status - a status code described in this class header
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	/**
	 * This method returns if a comment has been submitted for this booking
	 * @return comment status
	 */
	public boolean getCommentMade() {
		return commentMade;
	}
	
	/**
	 * This method sets the booking as commented
	 */
	public void setCommentMade() {
		this.commentMade = true;
	}

	/**
	 * This method returns the client that made the book
	 * @return client
	 */
	public Client getClient() {
		return this.client;
	}
	
	/**
	 * This method sets the client
	 * @param client - new client associated to this book
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
	 * Get the offer object
	 * @return Offer object
	 */
	public Offer getOffer() {
		return this.offer;
	}
	
	/**
	 * Set a offer object 
	 * @param Offer object
	 */
	public void setOffer(Offer offer) {
		this.offer = offer;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Book)
			return (bookCode.equals(((Book) obj).bookCode));
		return false;
	}
	
	@Override
	public String toString() {
		return bookCode;
	}
	
}