package domain;

import java.io.Serializable;
import java.util.Date;

import configuration.Config;

/**
 * Clase de oferta
 */
public class Offer implements Comparable<Offer>, Serializable {
	
	private static final long serialVersionUID = 1L;
	private static int lastOfferNumber = 0;
	
	private int offerNumber;
	private Date firstDay;
	private Date lastDay;
	private float price;
	private Book book;
	private RuralHouse ruralHouse;

	/**
	 * Default constructor
	 * @param ruralHouse
	 * @param firstDay
	 * @param lastDay
	 * @param price
	 */
	public Offer(RuralHouse ruralHouse, Date firstDay, Date lastDay, float price) {
		this.offerNumber = ++lastOfferNumber;
		this.firstDay = firstDay;
		this.lastDay = lastDay;
		this.price = price;
		this.book = null;
		this.ruralHouse = ruralHouse;
	}
	
	/**
	 * Get the house of the offer
	 * @return the rural house object
	 */
	public RuralHouse getRuralHouse() {
		return this.ruralHouse;
	}

	/**
	 * Set the house to an offer
	 * @param ruralHouse - Rural house object
	 */
	public void setRuralHouse(RuralHouse ruralHouse) {
		this.ruralHouse = ruralHouse;
	}

	/**
	 * Get the offer number
	 * @return offer number
	 */
	public int getOfferNumber() {
		return this.offerNumber;
	}

	/**
	 * Get the first day of the offer
	 * @return the first day
	 */
	public Date getFirstDay() {
		return this.firstDay;
	}

	/**
	 * Set the first day of the offer
	 * @param firstDay - The first day
	 */
	public void setFirstDay(Date firstDay) {
		this.firstDay = firstDay;
	}

	/**
	 * Get the last day of the offer
	 * @return the last day
	 */
	public Date getLastDay() {
		return this.lastDay;
	}

	/**
	 * Set the last day of the offer
	 * @param lastDay - The last day
	 */
	public void setLastDay(Date lastDay) {
		this.lastDay = lastDay;
	}

	/**
	 * Get the price
	 * @return price
	 */
	public float getPrice() {
		return this.price;
	}

	/**
	 * Set the price
	 * @param price - new price of the offer
	 */
	public void setPrice(float price) {
		this.price = price;
	}

	/**
	 * Get the book object
	 * @return book object
	 */
	public Book getBook() {
		return this.book;
	}

	/**
	 * Set the book object
	 * @param book - Book object
	 */
	public void setBook(Book book) {
		this.book = book;
	}
	
	/**
	 * This method creates a book with no client information associated
	 * @return a Book object
	 */
	public Book createBook() {
		book = new Book(this);
		return book;
	}
	
	/**
	 * This method creates a book of the current offer assigned to client
	 * @param client - the client that makes the booking
	 * @return a Book object
	 */
	public Book createBook(Client client) {
		book = new Book(client, this);
		return book;
	}

	@Override
	public int compareTo(Offer o) {
		return Integer.compare(offerNumber, o.offerNumber);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Offer)
			return (offerNumber == ((Offer) obj).offerNumber);
		return false;
	}
	
	@Override
	public String toString() {
		return "Oferta " + offerNumber + ": " + 
				"del " + Config.getShortDateFormat().format(firstDay) + " al " + Config.getShortDateFormat().format(lastDay) +
				" por " + Config.getDecimalFormat().format(price) + "€";
	}

	/**
	 * @return the lastOfferNumber
	 */
	public static int getLastOfferNumber() {
		return lastOfferNumber;
	}

	/**
	 * @param lastOfferNumber - the lastOfferNumber to set
	 */
	public static void setLastOfferNumber(int lastOfferNumber) {
		Offer.lastOfferNumber = lastOfferNumber;
	}
	
}