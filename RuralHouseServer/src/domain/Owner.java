package domain;

import java.io.Serializable;
import java.util.Vector;

/**
 * Clase de propietario
 */
public class Owner extends User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String bankAccount;
	private Vector<RuralHouse> ruralHouses;

	/**
	 * Default constructor
	 * @param email
	 * @param password
	 * @param telephone
	 * @param bankAccount
	 */
	public Owner(String email, char[] password, String telephone, String bankAccount) {
		super(email, password, telephone);
		this.bankAccount = bankAccount;
		this.ruralHouses = new Vector<RuralHouse>();
	}
	
	/**
	 * This method returns the owner bank account number
	 * @return The bank account number
	 */
	public String getBankAccount() {
		return this.bankAccount;
	}

	/**
	 * This method sets the owner account number 
	 * @param bankAccount
	 *            bank account number
	 */
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	
	/**
	 * This method obtains an owner's rural houses 
	 * @return a vector of Rural Houses
	 */
	public Vector<RuralHouse> getRuralHouses() {
		return ruralHouses;
	}
	
	/**
	 * This method sets an owner's rural houses 
	 * @param ruralHouses
	 *            a vector of Rural Houses
	 */
	public void setRuralHouses(Vector<RuralHouse> ruralHouses) {
		this.ruralHouses = ruralHouses;
	}
	
	/**
	 * Adds a new Rural House to the owner
	 * @param description
	 * 			Text description of the house
	 * @param location
	 * 			Location of the house
	 * @param numbers
	 * 			Number of rooms, kitchens, baths, etc.
	 * @param pictures
	 * 			Vector of pictures of the house
	 * @return The just created Rural House object
	 */
	public RuralHouse addRuralHouse(String description, String location, int rooms, int kitchens, int baths, int livings, int parkings, Vector<byte[]> pictures) {
		RuralHouse casa = new RuralHouse(description, location, rooms, kitchens, baths, livings, parkings, pictures, this);
		ruralHouses.add(casa);
		return casa;
	}
	
	/**
	 * Removes a Rural House from the list of the owner's rural houses
	 * @param ruralHouse - the house to be removed
	 * @return true if the object has been removed
	 */
	public boolean removeRuralHouse(RuralHouse ruralHouse) {
		return ruralHouses.remove(ruralHouse);
	}
	
}