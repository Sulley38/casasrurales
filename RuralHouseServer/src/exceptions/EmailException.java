package exceptions;

public class EmailException extends Exception {
	private static final long serialVersionUID = 1L;

	public EmailException() {
		super();
	}

	/**
	 * This exception is triggered when an email can't be sent
	 * 
	 * @param String
	 * @return None
	 */
	public EmailException(String s) {
		super(s);
	}
}
