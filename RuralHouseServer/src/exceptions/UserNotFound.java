package exceptions;

public class UserNotFound extends Exception {
	private static final long serialVersionUID = 1L;

	public UserNotFound() {
		super();
	}

	/**
	 * This exception is triggered if user is not found with the given user/password combination
	 * @param String
	 */
	public UserNotFound(String s) {
		super(s);
	}
}
