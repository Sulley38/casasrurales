package exceptions;

public class OfferNotFound extends Exception {
	private static final long serialVersionUID = 1L;

	public OfferNotFound() {
		super();
	}

	/**
	 * This exception is triggered if a requested offer is not found
	 * @param String
	 */
	public OfferNotFound(String s) {
		super(s);
	}
}
