package exceptions;

public class BookNotFound extends Exception {
	private static final long serialVersionUID = 1L;

	public BookNotFound() {
		super();
	}

	/**
	 * This exception is triggered if a requested book is not found
	 * @param String
	 */
	public BookNotFound(String s) {
		super(s);
	}
}
