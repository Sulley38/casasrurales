package dataAccess;

import java.io.File;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.config.Configuration;

import configuration.Config;
import domain.*;

public class DB4oManager { 
	
	// Persistencia para los identificadores de casa y oferta más recientes
	private static class NumberStorage {
		private int lastHouseNumber = 0, lastOfferNumber = 0;
	}
	
	private ObjectContainer db;
	private NumberStorage numeros;
	
	/**
	 * Inicia la conexión con la base de datos db4o.
	 */
	public DB4oManager() {
		Configuration dbConfig = Db4o.newConfiguration();
		dbConfig.updateDepth(2);
		if (Config.getDataBaseOpenMode()) {
			// Abrir fichero db4o
			db = Db4o.openFile(dbConfig, Config.getDb4oFilename());
			// Cargar números de casa y oferta
			List<NumberStorage> res = db.query(NumberStorage.class);
			numeros = (res.isEmpty()) ? new NumberStorage() : res.get(0);
			RuralHouse.setLastHouseNumber(numeros.lastHouseNumber);
			Offer.setLastOfferNumber(numeros.lastOfferNumber);
		} else {
			// Eliminar fichero anterior
			new File(Config.getDb4oFilename()).delete();
			// Abrir fichero nuevo
			db = Db4o.openFile(dbConfig, Config.getDb4oFilename());
			// Introducir contenidos en la base de datos
			numeros = new NumberStorage();
			Owner admin = new Owner("admin@ehu.es", "admin".toCharArray(), "123456789", "12345678901234567890");
			db.store(new Client("cliente@ehu.es", "cliente".toCharArray(), "987654321"));
			db.store(admin.addRuralHouse("Amplia y vistosa", "Pasaia", 5, 2, 3, 3, 1, new Vector<byte[]>()));
			db.store(admin.addRuralHouse("Rústica", "Donostia", 4, 2, 2, 4, 2, new Vector<byte[]>()));
			db.store(admin);
			db.commit();
		}
	}
	
	/**
	 * Devuelve la versión almacenada originalmente en la base de datos equivalente al objeto indicado.
	 * @param prototipo - objeto a buscar en la base de datos
	 * @param clase - nombre de la clase del objeto
	 * @return el objeto conectado, recién sacado de la base de datos
	 */
	private <T> T obtenerObjetoOriginal(T prototipo, Class<T> clase) {
		for (T obj : db.query(clase))
			if (obj.equals(prototipo))
				return obj;
		return null;
	}
	
	/**
	 * Termina la conexión con la base de datos, guardando todos los cambios.
	 */
	public void cerrarBD() {
		db.close();
	}
	
	/***************************/
	/* OPERACIONES SOBRE CASAS */
	/***************************/
	
	public RuralHouse crearCasaRural(String description, String location, int rooms, int kitchens, int baths, int livings, int parkings, Vector<byte[]> pictures, Owner owner) {
		Owner realOwner = obtenerObjetoOriginal(owner, Owner.class);
		if (realOwner == null)
			return null;
		
		RuralHouse newRuralHouse = realOwner.addRuralHouse(description, location, rooms, kitchens, baths, livings, parkings, pictures);
		numeros.lastHouseNumber++;
		db.store(newRuralHouse);
		db.store(realOwner);
		db.store(numeros);
		db.commit();
		return newRuralHouse;
	}
	
	public boolean eliminarCasaRural(RuralHouse ruralHouse) {
		RuralHouse realRuralHouse = obtenerObjetoOriginal(ruralHouse, RuralHouse.class);
		if (realRuralHouse == null)
			return false;
		
		boolean eliminada = realRuralHouse.getOwner().removeRuralHouse(realRuralHouse);
		if (eliminada) {
			// Eliminar explícitamente las imágenes y los vectores
			for (byte[] img : realRuralHouse.getPictures())
				db.delete(img);
			db.delete(realRuralHouse.getPictures());
			db.delete(realRuralHouse.getOffers());
			db.delete(realRuralHouse.getComments());
			db.delete(realRuralHouse);
			db.store(realRuralHouse.getOwner());
			db.commit();
		}
		return eliminada;
	}
	
	public boolean actualizarCasaRural(RuralHouse ruralHouse, String description, int rooms, int kitchens, int baths, int livings, int parkings, Vector<byte[]> pictures) {
		RuralHouse realRuralHouse = obtenerObjetoOriginal(ruralHouse, RuralHouse.class);
		if (realRuralHouse == null)
			return false;
		
		realRuralHouse.setDescription(description);
		realRuralHouse.setRooms(rooms);
		realRuralHouse.setKitchens(kitchens);
		realRuralHouse.setBaths(baths);
		realRuralHouse.setLivings(livings);
		realRuralHouse.setParkings(parkings);
		realRuralHouse.setPictures(pictures);
		db.store(realRuralHouse);
		db.commit();
		return true;
	}
	
	public List<RuralHouse> listadoCasasRurales() {
		return db.query(RuralHouse.class);
	}
	
	/*****************************/
	/* OPERACIONES SOBRE OFERTAS */
	/*****************************/
	
	public Offer crearOferta(RuralHouse ruralHouse, Date firstDay, Date lastDay, float price) {
		RuralHouse realRuralHouse = obtenerObjetoOriginal(ruralHouse, RuralHouse.class);
		if (realRuralHouse == null)
			return null;
		
		Offer newOffer = realRuralHouse.createOffer(firstDay, lastDay, price);
		numeros.lastOfferNumber++;
		db.store(newOffer);
		db.store(realRuralHouse);
		db.store(numeros);
		db.commit();
		return newOffer;
	}
	
	public boolean eliminarOferta(Offer offer) {
		Offer realOffer = obtenerObjetoOriginal(offer, Offer.class);
		if (realOffer == null)
			return false;
		
		boolean eliminada = realOffer.getRuralHouse().removeOffer(realOffer);
		if (eliminada) {
			db.delete(realOffer);
			db.store(realOffer.getRuralHouse());
			db.commit();
		}
		return eliminada;
	}
	
	public List<Offer> listadoOfertas() {
		return db.query(Offer.class);
	}
	
	/******************************/
	/* OPERACIONES SOBRE RESERVAS */
	/******************************/
	
	public Book crearReserva(Offer offer, Client client, int status) {
		Offer realOffer = obtenerObjetoOriginal(offer, Offer.class);
		if (realOffer == null)
			return null;
		
		Book newBook;
		if (client == null) {
			// Crear reserva anónima
			newBook = realOffer.createBook();
		} else {
			// Crear reserva asociada a un cliente
			Client realClient = obtenerObjetoOriginal(client, Client.class);
			newBook = realOffer.createBook(realClient);
			db.store(realClient);
		}
		newBook.setStatus(status);
		db.store(newBook);
		db.store(realOffer);
		db.commit();
		return newBook;
	}
	
	public boolean eliminarReserva(Book book) {
		Book realBook = obtenerObjetoOriginal(book, Book.class);
		if (realBook == null)
			return false;
		
		realBook.getOffer().setBook(null);
		if (realBook.getClient() != null) {
			realBook.getClient().removeBooking(realBook);
			db.store(realBook.getClient());
		}
		db.delete(realBook);
		db.store(realBook.getOffer());
		db.commit();
		return true;
	}
	
	public boolean actualizarReserva(Book book, int status) {
		Book realBook = obtenerObjetoOriginal(book, Book.class);
		if (realBook == null)
			return false;
		
		realBook.setStatus(status);
		db.store(realBook);
		db.commit();
		return true;
	}
	
	public List<Book> listadoReservas() {
		return db.query(Book.class);
	}
	
	/*********************************/
	/* OPERACIONES SOBRE COMENTARIOS */
	/*********************************/
	
	public Comment crearComentario(Book book, String text, float rating) {
		Book realBook = obtenerObjetoOriginal(book, Book.class);
		if (realBook == null)
			return null;
		
		Comment comentario = realBook.getOffer().getRuralHouse().addComment(text, rating);
		realBook.setCommentMade();
		db.store(comentario);
		db.store(realBook);
		db.store(realBook.getOffer().getRuralHouse());
		db.commit();
		return comentario;
	}
	
	public boolean eliminarComentario(Comment comment) throws RemoteException {
		Comment realComment = obtenerObjetoOriginal(comment, Comment.class);
		if (realComment == null)
			return false;
		
		boolean eliminado = realComment.getRuralHouse().removeComment(realComment);
		if (eliminado) {
			db.delete(realComment);
			db.store(realComment.getRuralHouse());
			db.commit();
		}
		return eliminado;
	}
	
	/****************************************/
	/* OPERACIONES SOBRE CUENTAS DE USUARIO */
	/****************************************/
	
	public void registrarCliente(String email, char[] password, String phone) {
		db.store(new Client(email, password, phone));
		db.commit();
	}
	
	public void registrarPropietario(String email, char[] password, String phone, String bankAccount) {
		db.store(new Owner(email, password, phone, bankAccount));
		db.commit();
	}
	
	public List<User> listadoUsuarios() {
		return db.query(User.class);
	}
	
	public boolean actualizarUsuario(User user, char[] password, String phone, String bankAccount) {
		User realUser = obtenerObjetoOriginal(user, User.class);
		if (realUser == null)
			return false;
		
		if (password != null)
			realUser.setPassword(password);
		realUser.setPhone(phone);
		if (user instanceof Owner)
			((Owner) realUser).setBankAccount(bankAccount);
		db.store(realUser);
		db.commit();
		return true;
	}

}
	
