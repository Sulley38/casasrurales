package businessLogic;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Vector;

import configuration.Config;
import domain.Book;
import domain.Comment;
import domain.Offer;
import exceptions.EmailException;

public class EmailService {

	public EmailService() {
	}
	
	// Devuelve un texto para insertar al final de cualquier mensaje
	private String firmaDeMensaje() {
		return "Reciba un cordial saludo,\r\n" +
				"El equipo de Sinking Software.\r\n\r\n" +
				"---------------------------------------------\r\n" +
				"No conteste a este mensaje, ha sido generado de forma automática.";
	}
	
	/**
	 * Texto de bienvenida personalizado con el e-mail de registro.
	 * @param email - correo electrónico de registro
	 * @return una cadena de texto
	 */
	public String textoDeBienvenida(String email) {
		return "    ¡Bienvenido, " + email + "!\r\n\r\n" +
				"Gracias por registrarse en nuestra aplicación de gestión de casas rurales.\r\n" +
				"Su cuenta acaba de ser activada. Ya puede iniciar sesión desde la aplicación.\r\n" +
				"Le recordamos que, por su seguridad, nunca debe revelar a nadie su clave de acceso.\r\n\r\n" +
				"Le deseamos una feliz estancia,\r\n" +
				"El equipo de Sinking Software.\r\n\r\n" +
				"---------------------------------------------\r\n" +
				"No conteste a este mensaje, ha sido generado de forma automática.";
	}
	
	/**
	 * Texto para el cliente de reserva realizada con datos sobre la oferta.
	 * @param reserva - reserva realizada
	 * @return una cadena de texto
	 */
	public String textoClienteReservaRealizada(Book reserva) {
		String texto = "    Estimado cliente,\r\n\r\n" +
			"Este mensaje confirma su reserva con código " + reserva.getBookCode() + " realizada el " + Config.getShortDateFormat().format(reserva.getBookDate()) + " para la oferta\r\n" +
			"    " + reserva.getOffer().toString() + " de la casa en " + reserva.getOffer().getRuralHouse().getCity() + "\r\n\r\n";
		if (reserva.getStatus() == Book.PENDING)
			texto += "Recuerde: debe ingresar el depósito de " + Config.getDecimalFormat().format(reserva.getOffer().getPrice() * 0.3f) + "€ en la cuenta " +
					reserva.getOffer().getRuralHouse().getOwner().getBankAccount() + " en menos de 72 horas para que su reserva no sea desestimada.\r\n" +
					"Puede cancelar su reserva hasta un día antes de la fecha de entrada, pero sólo recuperaría el depósito de hacerlo con una semana o más de antelación.\r\n\r\n";
		else
			texto += "Recuerde: deberá abonar la cantidad de " + Config.getDecimalFormat().format(reserva.getOffer().getPrice()) + "€ el día de la entrada al llegar a la casa.\r\n" +
					"Puede cancelar su reserva hasta un día antes de la fecha de entrada.\r\n\r\n";
		return texto + firmaDeMensaje();
	}
	
	/**
	 * Texto para el propietario de reserva realizada con datos sobre la oferta, el cliente, y las oferas solapadas.
	 * @param reserva - reserva realizada
	 * @param conflictos - ofertas que entran en conflicto
	 * @return una cadena de texto
	 */
	public String textoPropietarioReservaRealizada(Book reserva, Vector<Offer> conflictos) {
		String texto = "    Estimado propietario,\r\n\r\n" +
			"Le comunicamos que su oferta\r\n" +
			"    " + reserva.getOffer().toString() + " de la casa *" + reserva.getOffer().getRuralHouse().toString() + "*\r\n" +
			"ha sido reservada por " + ((reserva.getClient() != null) ? ("el cliente " + reserva.getClient().toString() + ".\r\n") : "un cliente anónimo.\r\n") +
			"Puede comprobar los detalles y modificar el estado de la reserva en su panel de gestión.\r\n\r\n";
		if (reserva.getStatus() == Book.PENDING) {
			texto += "Debería recibir un depósito de " + Config.getDecimalFormat().format(reserva.getOffer().getPrice() * 0.3f) + "€ en su cuenta corriente antes de 72 horas.\r\n" +
					"De no ser así, puede anular la reserva desde la aplicación para que la oferta vuelva a estar disponible.\r\n" +
					"Le rogamos que marque el depósito pagado desde su panel de gestión en cuanto tenga constancia del ingreso.\r\n\r\n";
		}
		if (conflictos.size() > 0) {
			texto += "Adicionalmente, las siguientes ofertas han sido eliminadas de la aplicación por entrar en conflicto con la oferta reservada:\r\n";
			for (Offer o : conflictos)
				texto += "    " + o.toString() + "\r\n";
			texto += "\r\n";
		}
		return texto + firmaDeMensaje();
	}
	
	/**
	 * Texto para el cliente cuando se cancela una reserva suya.
	 * @param reserva - reserva cancelada
	 * @return una cadena de texto
	 */
	public String textoClienteReservaCancelada(Book reserva) {
		return "    Estimado cliente,\r\n\r\n" +
			"Le informamos de que su reserva " + reserva.getBookCode() + " con fecha " + Config.getShortDateFormat().format(reserva.getBookDate()) + " y los siguientes datos de oferta:\r\n" +
			"    " + reserva.getOffer().toString() + " de la casa en " + reserva.getOffer().getRuralHouse().getCity() + "\r\n" +
			"acaba de ser CANCELADA.\r\n\r\n" +
			"Si usted ha cancelado la reserva, este mensaje confirma que el proceso se ha llevado con éxito y se le reembolsará la cantidad que haya abonado siempre que proceda con las normas de cancelación establecidas.\r\n" +
			"Si no esperaba esta cancelación, puede ponerse en contacto con el propietario de la casa a través de la dirección de correo electrónico " +
			reserva.getOffer().getRuralHouse().getOwner().getEmail() + " o en el teléfono " + reserva.getOffer().getRuralHouse().getOwner().getPhone() + ".\r\n\r\n" +
			firmaDeMensaje();
	}
	
	/**
	 * Texto para el propietario cuando se cancela una reserva en una de sus casas.
	 * @param reserva - reserva cancelada
	 * @return una cadena de texto
	 */
	public String textoPropietarioReservaCancelada(Book reserva) {
		return "    Estimado propietario,\r\n\r\n" +
			"Le informamos de que la reserva " + reserva.getBookCode() + " con fecha " + Config.getShortDateFormat().format(reserva.getBookDate()) + ", datos de oferta\r\n" + 
			"    " + reserva.getOffer().toString() + " de la casa *" + reserva.getOffer().getRuralHouse().toString() + "*\r\n" +
			"y realizada por " + ((reserva.getClient() != null) ? ("el cliente " + reserva.getClient().toString()) : "un cliente anónimo") + " ha sido CANCELADA.\r\n\r\n" +
			"Esta cancelación puede deberse a una eliminación de la casa o la oferta del sistema, una anulación manual por su parte, o una cancelación por parte del cliente.\r\n" +
			"Si no ha eliminado la oferta del sistema, ésta vuelve a aparecer disponible para otros clientes.\r\n" +
			"Tenga presente que si este mensaje llega con una semana o más de antelación a la fecha de inicio de la oferta deberá devolver la cantidad que el cliente haya abonado hasta el momento.\r\n\r\n" +
			firmaDeMensaje();
	}
	
	/**
	 * Texto para el cliente cuando se actualiza el estado de una reserva suya.
	 * @param reserva - reserva actualizada
	 * @param estado - nuevo estado de la reserva
	 * @return una cadena de texto
	 */
	public String textoClienteEstadoActualizado(Book reserva, int estado) {
		return "    Estimado cliente,\r\n\r\n" +
			"El estado de su reserva " + reserva.getBookCode() + " con fecha " + Config.getShortDateFormat().format(reserva.getBookDate()) + " y los siguientes datos de oferta:\r\n" +
			"    " + reserva.getOffer().toString() + " de la casa en " + reserva.getOffer().getRuralHouse().getCity() + "\r\n" +
			"ha sido actualizado a " + Book.statusText[estado] + ".\r\n" +
			"Si cree que se trata de un error, puede ponerse en contacto con el propietario de la casa a través de la dirección de correo electrónico " +
			reserva.getOffer().getRuralHouse().getOwner().getEmail() + " o en el teléfono " + reserva.getOffer().getRuralHouse().getOwner().getPhone() + ".\r\n\r\n" +
			firmaDeMensaje();
	}
	
	/**
	 * Texto para el propietario cuando se hace un comentario sobre una de sus casas.
	 * @param comentario - comentario escrito
	 * @return una cadena de texto
	 */
	public String textoPropietarioComentarioPublicado(Comment comentario) {
		return "    Estimado propietario,\r\n\r\n" +
			"Un comentario acaba de ser publicado acerca de su casa rural\r\n" +
			"    " + comentario.getRuralHouse().toString() + "\r\n" +
			"cuyo contenido es:\r\n" +
			"    " + comentario.getText() + "\r\n\r\n" +
			"Puede moderar los comentarios accediendo al panel de gestión de casas rurales.\r\n\r\n" +
			firmaDeMensaje();
	}
	
	/**
	 * Envia un correo electrónico al destinatario con el asunto y mensaje indicados, codificado en UTF-8.
	 * @param destinatario - dirección de destino del correo (RFC 2822)
	 * @param asunto - título del correo (RFC 2047)
	 * @param mensaje - contenido del correo, en texto plano y separando líneas con '\r\n'
	 * @return true si el correo ha sido aceptado para reparto (no garantiza que llegue a destino)
	 * @throws EmailException
	 */
	public boolean enviarCorreo(String destinatario, String asunto, String mensaje) throws EmailException {
		try {
			String url;
			try {
				url = "http://ivanma.com/sinkingmailer.php?para=" + URLEncoder.encode(destinatario, "UTF-8") +
						"&asunto=" + URLEncoder.encode(asunto, "UTF-8") + "&mensaje=" + URLEncoder.encode(mensaje, "UTF-8");
			} catch (UnsupportedEncodingException e1) {
				url = "http://ivanma.com/sinkingmailer.php?para=" + destinatario +
						"&asunto=" + asunto + "&mensaje=" + mensaje;
			}
			// Realizar petición al servidor de Iván
			return (new URL(url).openStream().read() == '1');
		} catch (IOException e) {
			throw new EmailException("Error en la conexión con el servidor de correo: " + e.getMessage());
		}
	}
	
}
