package businessLogic;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Vector;

import configuration.Config;

import dataAccess.DB4oManager;
import domain.*;
import exceptions.BookNotFound;
import exceptions.EmailException;
import exceptions.OfferCanNotBeBooked;
import exceptions.OfferNotFound;
import exceptions.UserNotFound;

public class FacadeImplementation extends UnicastRemoteObject implements ApplicationFacadeInterface {

	private static final long serialVersionUID = 1L;
	private DB4oManager accesoDatos;
	private EmailService correo;

	public FacadeImplementation() throws RemoteException {
		super(Config.getPortRMI());
		accesoDatos = new DB4oManager();
		correo = new EmailService();
	}

	/***************************/
	/* OPERACIONES SOBRE CASAS */
	/***************************/
	
	public void createRuralHouse(String description, String location, int rooms,
			int kitchens, int baths, int livings, int parkings, Vector<byte[]> pictures, Owner owner) throws RemoteException {
		accesoDatos.crearCasaRural(description, location, rooms, kitchens, baths, livings, parkings, pictures, owner);
	}
	
	public void deleteRuralHouse(RuralHouse ruralHouse) throws RemoteException {
		Vector<Offer> ofertas = new Vector<Offer>(ruralHouse.getOffers());
		for (Offer offer : ofertas)
			removeOffer(offer);
		Vector<Comment> comentarios = new Vector<Comment>(ruralHouse.getComments());
		for (Comment comment : comentarios)
			deleteComment(comment);
		accesoDatos.eliminarCasaRural(ruralHouse);
	}
	
	public void updateRuralHouse(RuralHouse ruralHouse, String description,
			int rooms, int kitchens, int baths, int livings, int parkings, Vector<byte[]> pictures) throws RemoteException {
		accesoDatos.actualizarCasaRural(ruralHouse, description, rooms, kitchens, baths, livings, parkings, pictures);
	}
	
	public Vector<RuralHouse> getRuralHouses(String city) throws RemoteException {
		Vector<RuralHouse> result = new Vector<RuralHouse>();
		for (RuralHouse r : accesoDatos.listadoCasasRurales())
			if (r.getCity().equals(city))
				result.add(r);
		Collections.sort(result);
		return result;
	}

	public Vector<RuralHouse> getRuralHouses(Owner owner) throws RemoteException {
		Vector<RuralHouse> result = new Vector<RuralHouse>();
		for (RuralHouse r : accesoDatos.listadoCasasRurales())
			if (r.getOwner().equals(owner))
				result.add(r);
		Collections.sort(result);
		return result;
	}
	
	public Vector<String> getAllCities() throws RemoteException {
		HashSet<String> ciudades = new HashSet<String>();
		for (RuralHouse r : accesoDatos.listadoCasasRurales())
			ciudades.add(r.getCity());
		Vector<String> result = new Vector<String>(ciudades);
		Collections.sort(result);
		return result;
	}

	/*****************************/
	/* OPERACIONES SOBRE OFERTAS */
	/*****************************/
	
	public void createOffer(RuralHouse ruralHouse, Date firstDay, Date lastDay, float price) throws RemoteException {
		accesoDatos.crearOferta(ruralHouse, firstDay, lastDay, price);
	}

	public void removeOffer(Offer offer) throws RemoteException {
		if (offer.getBook() != null)
			deleteBook(offer.getBook());
		accesoDatos.eliminarOferta(offer);
	}
	
	public Offer getOffer(int offerNumber) throws RemoteException, OfferNotFound {
		for (Offer f : accesoDatos.listadoOfertas())
			if (f.getOfferNumber() == offerNumber)
				return f;
		throw new OfferNotFound("No se encuentra la oferta asociada a ese código");
	}
	
	/******************************/
	/* OPERACIONES SOBRE RESERVAS */
	/******************************/
	
	public Book createBook(Offer offer, Client client) throws RemoteException, OfferCanNotBeBooked {
		// Comprobar que la oferta no esté reservada
		if (offer.getBook() != null)
			throw new OfferCanNotBeBooked("La oferta ya ha sido reservada");
		// Comprobar que la fecha no haya pasado
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
		cal.set(Calendar.MILLISECOND, 0);
		if (offer.getFirstDay().compareTo(cal.getTime()) < 0)
			throw new OfferCanNotBeBooked("La oferta ya ha pasado");
		// Buscar y eliminar las ofertas en conflicto con la reserva
		Vector<Offer> ofertasSolapadas = new Vector<Offer>(offer.getRuralHouse().findConflictingOffers(offer));
		for (Offer oferta : ofertasSolapadas)
			removeOffer(oferta);
		// Si quedan menos de tres días, crear reserva con estado 'a pagar en la casa'
		cal.add(Calendar.DAY_OF_MONTH, 3);
		Book book;
		if (offer.getFirstDay().compareTo(cal.getTime()) <= 0)
			book = accesoDatos.crearReserva(offer, client, Book.PAY_AT_CHECKIN);
		else
			book = accesoDatos.crearReserva(offer, client, Book.PENDING);
		try {
			// Enviar e-mail al cliente con los datos de la reserva
			if (client != null)
				correo.enviarCorreo(client.getEmail(), "Reserva realizada", correo.textoClienteReservaRealizada(book));
			// Enviar e-mail al propietario cuando le han hecho una reserva (y si ha perdido ofertas en conflicto)
			correo.enviarCorreo(offer.getRuralHouse().getOwner().getEmail(), "Reserva realizada", correo.textoPropietarioReservaRealizada(book, ofertasSolapadas));
		} catch (EmailException e) {
			System.err.println(e.getMessage());
		}
		return book;
	}
	
	public void deleteBook(Book book) throws RemoteException {
		if (book.getOffer().getFirstDay().compareTo(new Date()) > 0) {
			// Avisos cuando se trata de una reserva futura
			try {
				if (book.getClient() != null) {
					// Enviar e-mail al cliente comunicándole la cancelación de su reserva
					correo.enviarCorreo(book.getClient().getEmail(), "Cancelación de su reserva", correo.textoClienteReservaCancelada(book));
				}
				correo.enviarCorreo(book.getOffer().getRuralHouse().getOwner().getEmail(), "Cancelación de la reserva", correo.textoPropietarioReservaCancelada(book));
			} catch (EmailException e) {
				System.err.println(e.getMessage());
			}
		}
		accesoDatos.eliminarReserva(book);
	}
	
	public void updateBook(Book book, int status) throws RemoteException {
		if (book.getClient() != null) {
			try {
				// Enviar e-mail informando de la actualización de estado
				correo.enviarCorreo(book.getClient().getEmail(), "Actualización de su reserva", correo.textoClienteEstadoActualizado(book, status));
			} catch (EmailException e) {
				System.err.println(e.getMessage());
			}
		}
		accesoDatos.actualizarReserva(book, status);
	}

	public Book getBook(String code) throws RemoteException, BookNotFound {
		for (Book b : accesoDatos.listadoReservas())
			if (b.getBookCode().equals(code))
				return b;
		throw new BookNotFound("No se encuentra la reserva asociada a ese código");
	}
	
	public Vector<Book> getBooks(Owner owner) throws RemoteException {
		Vector<Book> reservas = new Vector<Book>();
		for (Book b : accesoDatos.listadoReservas())
			if (b.getOffer().getRuralHouse().getOwner().equals(owner))
				reservas.add(b);
		return reservas;
	}
	
	public Vector<Book> getFutureBooks(Client client) throws RemoteException {
		Vector<Book> reservas = new Vector<Book>();
		for (Book b : accesoDatos.listadoReservas())
			if (b.getClient() != null && b.getClient().equals(client) && b.getOffer().getFirstDay().compareTo(new Date()) > 0)
				reservas.add(b);
		return reservas;
	}
	
	public Vector<Book> getPastUncommentedBooks(Client client) throws RemoteException {
		Vector<Book> reservas = new Vector<Book>();
		for (Book b : accesoDatos.listadoReservas())
			if (b.getClient() != null && b.getClient().equals(client) && !b.getCommentMade()
				&& b.getOffer().getLastDay().compareTo(new Date()) < 0)
				reservas.add(b);
		return reservas;
	}
	
	/*********************************/
	/* OPERACIONES SOBRE COMENTARIOS */
	/*********************************/
	
	public void addComment(Book book, String text, float rating) throws RemoteException {
		Comment comentario = accesoDatos.crearComentario(book, text, rating);
		try {
			// Enviar e-mail al propietario avisando del nuevo comentario
			correo.enviarCorreo(book.getOffer().getRuralHouse().getOwner().getEmail(), "Publicado un comentario", correo.textoPropietarioComentarioPublicado(comentario));
		} catch (EmailException e) {
			System.err.println(e.getMessage());
		}
	}
	
	public void deleteComment(Comment comment) throws RemoteException {
		accesoDatos.eliminarComentario(comment);
	}
	
	/****************************************/
	/* OPERACIONES SOBRE CUENTAS DE USUARIO */
	/****************************************/
	
	public void registerNewClient(String email, char[] password, String phone) throws RemoteException {
		accesoDatos.registrarCliente(email, password, phone);
		try {
			correo.enviarCorreo(email, "¡Bienvenido/a al mundo de las casas rurales!", correo.textoDeBienvenida(email));
		} catch (EmailException e) {
			System.err.println(e.getMessage());
		}
	}

	public void registerNewOwner(String email, char[] password, String phone, String bankAccount) throws RemoteException {
		accesoDatos.registrarPropietario(email, password, phone, bankAccount);
		try {
			correo.enviarCorreo(email, "¡Bienvenido/a al mundo de las casas rurales!", correo.textoDeBienvenida(email));
		} catch (EmailException e) {
			System.err.println(e.getMessage());
		}
	}
	
	public boolean emailExists(String email) throws RemoteException {
		for (User u : accesoDatos.listadoUsuarios())
			if (u.getEmail().equals(email))
				return true;
		// No hay usuario con ese e-mail
		return false;
	}
	
	public User login(String email, char[] password) throws RemoteException, UserNotFound {
		for (User u : accesoDatos.listadoUsuarios()) {
			if (u.getEmail().equals(email)) {
				if (Arrays.equals(u.getPassword(), password)) {
					// Devolver referencia al usuario
					return u;
				} else {
					// Contraseña incorrecta
					throw new UserNotFound("La contraseña introducida es incorrecta.");
				}
			}
		}
		// No hay usuario con ese e-mail
		throw new UserNotFound("No existe un usuario registrado con ese e-mail.");
	}

	public void updateUser(User user, char[] password, String phone, String bankAccount) throws RemoteException {
		accesoDatos.actualizarUsuario(user, password, phone, bankAccount);
	}

	
	// Operaciones sobre la base de datos, no exportadas en la interfaz
	
	/**
	 * Cierra la base de datos y guarda los cambios que se han efectuado en ella.
	 * @throws RemoteException
	 */
	public void closeDatabase() throws RemoteException {
		accesoDatos.cerrarBD();
	}
	
}
