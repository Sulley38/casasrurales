package businessLogic;

import java.io.IOException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import configuration.Config;

/**
 * Lanzar el servidor remoto.
 */
public class RemoteServer {
	
	private static FacadeImplementation server;
	
	public static void main(String[] args) {
		System.setProperty("java.security.policy", Config.getJavaPolicyPath());
		System.setProperty("java.rmi.server.hostname", Config.getExternalHostname());
		System.setSecurityManager(new RMISecurityManager());

		// Iniciar el servicio
		try {
			LocateRegistry.createRegistry(Config.getPortRMI());
			server = new FacadeImplementation();
			// Register the remote server
			String service = "//" + Config.getServerRMI() + ":" + Config.getPortRMI() + "/" + Config.getServiceRMI();
			Naming.rebind(service, server);
			System.out.println("Running service at:\n\t//" + Config.getExternalHostname() + ":" + Config.getPortRMI() + "/" + Config.getServiceRMI());
		} catch (RemoteException e1) {
			System.err.println(e1.toString());
			System.err.println("RMI registry already running.");
			System.exit(1);
		} catch (Exception e2) {
			System.err.println(e2.toString());
			System.exit(1);
		}
		
		// Esperar la tecla intro para salir
		System.out.println();
		System.out.println("Pulse ENTER para terminar la ejecución");
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Terminar el servicio
		try {
			server.closeDatabase();
		} catch (RemoteException e) {
			System.err.println(e.toString());
		}
		System.exit(0);
	}
	
}
