package businessLogic;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;
import java.util.Date;

import domain.*;
import exceptions.BookNotFound;
import exceptions.OfferCanNotBeBooked; 
import exceptions.OfferNotFound;
import exceptions.UserNotFound;

public interface ApplicationFacadeInterface extends Remote {

	/***************************/
	/* OPERACIONES SOBRE CASAS */
	/***************************/
	
	/**
	 * Añade una casa rural al sistema con las características que se pasan, y se la asigna al propietario indicado.
	 * @param description - descripción de la casa
	 * @param location - población de la casa
	 * @param rooms - número de habitaciones
	 * @param kitchens - número de cocinas
	 * @param baths - número de baños
	 * @param livings - número de salas de estar
	 * @param parkings - número de aparcamientos
	 * @param pictures - vector de imágenes
	 * @param owner - propietario de la casa
	 * @throws RemoteException
	 */
	public void createRuralHouse(String description, String location, int rooms,
			int kitchens, int baths, int livings, int parkings, Vector<byte[]> pictures, Owner owner) throws RemoteException;
	
	/**
	 * Elimina del sistema la casa rural pasada, incluyendo todas sus ofertas, reservas y comentarios.
	 * @param ruralHouse - casa a eliminar
	 * @throws RemoteException
	 */
	public void deleteRuralHouse(RuralHouse ruralHouse) throws RemoteException;
	
	/**
	 * Actualiza los detalles sobre una casa rural presente en el sistema.
	 * @param ruralHouse - casa a actualizar
	 * @param description - nueva descripción de la casa
	 * @param rooms - nuevo número de habitaciones
	 * @param kitchens - nuevo número de cocinas
	 * @param baths - nuevo número de baños
	 * @param livings - nuevo número de salas de estar
	 * @param parkings - nuevo número de aparcamientos
	 * @param pictures - nuevo vector de imágenes
	 * @throws RemoteException
	 */
	public void updateRuralHouse(RuralHouse ruralHouse, String description,
			int rooms, int kitchens, int baths, int livings, int parkings, Vector<byte[]> pictures) throws RemoteException;
	
	/**
	 * Obtiene todas las casas rurales de una población.
	 * @param city - localidad de la que buscar las casas
	 * @return un vector de casas rurales
	 * @throws RemoteException
	 */
	public Vector<RuralHouse> getRuralHouses(String city) throws RemoteException;
	
	/**
	 * Obtiene todas las casas rurales de un propietario.
	 * @param owner - propietario del que buscar las casas
	 * @return un vector de casas rurales
	 * @throws RemoteException
	 */
	public Vector<RuralHouse> getRuralHouses(Owner owner) throws RemoteException;
	
	/**
	 * Obtiene los nombres de todas las ciudades con casas rurales presentes en el sistema, sin repeticiones y en orden alfabético.
	 * @return un vector de nombres de ciudades
	 * @throws RemoteException
	 */
	public Vector<String> getAllCities() throws RemoteException;
	
	/*****************************/
	/* OPERACIONES SOBRE OFERTAS */
	/*****************************/
	
	/**
	 * Crea un paquete/oferta para la casa indicada con las características pasadas.
	 * @param ruralHouse - casa rural para la que se crea la oferta
	 * @param firstDay - fecha de inicio del paquete
	 * @param lastDay - fecha de fin del paquete
	 * @param price - precio del paquete
	 * @throws RemoteException
	 */
	public void createOffer(RuralHouse ruralHouse, Date firstDay, Date lastDay, float price) throws RemoteException;

	/**
	 * Elimina la oferta pasada, y su reserva si existiera.
	 * @param offer - oferta a eliminar
	 * @throws RemoteException
	 */
	public void removeOffer(Offer offer) throws RemoteException;
	
	/**
	 * Obtiene una oferta dado su identificador.
	 * @param offerNumber - identificador de la oferta buscada
	 * @return el objeto Offer correspondiente al identificador
	 * @throws RemoteException
	 * @throws OfferNotFound
	 */
	public Offer getOffer(int offerNumber) throws RemoteException, OfferNotFound;

	/******************************/
	/* OPERACIONES SOBRE RESERVAS */
	/******************************/
	
	/**
	 * Crea una reserva a nombre del cliente pasado para la oferta indicada.
	 * @param offer - oferta para la que crear la reserva
	 * @param client - cliente que hace la reserva, null si es una reserva anónima
	 * @return el objeto Book creado
	 * @throws RemoteException
	 * @throws OfferCanNotBeBooked
	 */
	public Book createBook(Offer offer, Client client) throws RemoteException, OfferCanNotBeBooked;
	
	/**
	 * Elimina la reserva indicada, notificando al cliente que la hizo si se dispone de información.
	 * @param book - reserva a eliminar
	 * @throws RemoteException
	 */
	public void deleteBook(Book book) throws RemoteException;

	/**
	 * Actualiza el estado de una reserva presente en el sistema.
	 * @param book - reserva a actualizar
	 * @param status - nuevo estado de la reserva
	 * @throws RemoteException
	 */
	public void updateBook(Book book, int status) throws RemoteException;
	
	/**
	 * Obtiene una reserva dado su código.
	 * @param code - código de la reserva buscada
	 * @return el objeto Book correspondiente al código
	 * @throws RemoteException
	 * @throws BookNotFound
	 */
	public Book getBook(String code) throws RemoteException, BookNotFound;
	
	/**
	 * Obtiene todas las reservas hechas en las casas rurales de un propietario.
	 * @param owner - propietario de las casas en las que buscar reservas
	 * @return un vector de reservas
	 * @throws RemoteException
	 */
	public Vector<Book> getBooks(Owner owner) throws RemoteException;
	
	/**
	 * Obtiene todas las reservas que aún no han comenzado de un cliente.
	 * @param client - cliente del que buscar reservas
	 * @return un vector de reservas
	 * @throws RemoteException
	 */
	public Vector<Book> getFutureBooks(Client client) throws RemoteException;
	
	/**
	 * Obtiene todas las reservas pasadas y sin comentar de un cliente.
	 * @param client - cliente del que buscar reservas
	 * @return un vector de reservas
	 * @throws RemoteException
	 */
	public Vector<Book> getPastUncommentedBooks(Client client) throws RemoteException;
	
	/*********************************/
	/* OPERACIONES SOBRE COMENTARIOS */
	/*********************************/
	
	/**
	 * Añade un comentario sobre la casa de la reserva indicada.
	 * @param book - reserva a la que corresponde el comentario
	 * @param text - texto del comentario
	 * @param rating - puntuación dada a la casa
	 * @throws RemoteException
	 */
	public void addComment(Book book, String text, float rating) throws RemoteException;
	
	/**
	 * Elimina el comentario indicado.
	 * @param comment - comentario a eliminar
	 * @throws RemoteException
	 */
	public void deleteComment(Comment comment) throws RemoteException;
	
	/****************************************/
	/* OPERACIONES SOBRE CUENTAS DE USUARIO */
	/****************************************/
	
	/**
	 * Crea un nuevo cliente y lo almacena en la base de datos.
	 * @param email - email del usuario
	 * @param password - contraseña del usuario
	 * @param phone - teléfono de contacto del usuario
	 * @throws RemoteException
	 */
	public void registerNewClient(String email, char[] password, String phone) throws RemoteException;
	
	/**
	 * Crea un nuevo propietario y lo almacena en la base de datos.
	 * @param email - email del usuario
	 * @param password - contraseña del usuario
	 * @param phone - teléfono de contacto del usuario
	 * @param bankAccount - cuenta bancaria del usuario
	 * @throws RemoteException
	 */
	public void registerNewOwner(String email, char[] password, String phone, String bankAccount) throws RemoteException;
	
	/**
	 * Comprueba si existe un usuario registrado con el email indicado.
	 * @param email - dirección de correo a comprobar
	 * @return true si el email pertenece a un usuario
	 * @throws RemoteException
	 */
	public boolean emailExists(String email) throws RemoteException;
	
	/**
	 * Inicia la sesión del usuario cuyos datos corresponden a los pasados por parámetros, devolviendo una referencia al usuario.
	 * @param email - dirección de correo electrónico del usuario
	 * @param password - contraseña del usuario
	 * @return el usuario que ha iniciado sesión
	 * @throws RemoteException
	 * @throws UserNotFound
	 */
	public User login(String email, char[] password) throws RemoteException, UserNotFound;
	
	/**
	 * Guarda la información actualizada del usuario indicado.
	 * @param user - usuario a actualizar
	 * @param password - nueva contraseña, null para mantener sin cambios
	 * @param phone - nuevo teléfono
	 * @param bankAccount - nueva cuenta bancaria (valor ignorado para un cliente)
	 * @throws RemoteException
	 */
	public void updateUser(User user, char[] password, String phone, String bankAccount) throws RemoteException;
	
}
