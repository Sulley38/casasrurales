package gui;

import java.awt.Cursor;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.JList;
import javax.swing.JScrollPane;

import domain.Owner;
import domain.RuralHouse;

public class RemoveRuralHousePanel extends ApplicationPage {

	private static final long serialVersionUID = 1L;
	
	private JList<RuralHouse> lstCasas;
	private DefaultListModel<RuralHouse> listModel;
	private JScrollPane scrollPane;
	
	/**
	 * Create the panel.
	 */
	public RemoveRuralHousePanel() {
		super("Eliminar una casa");
		
		JLabel lblAdvertencia = new JLabel("<html><span style='color:#FF0000;'>Advertencia:</span> al eliminar una casa se pierden todas las ofertas, reservas y comentarios asociados</html>");
		lblAdvertencia.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAdvertencia.setHorizontalAlignment(SwingConstants.CENTER);
		lblAdvertencia.setBounds(125, 65, 550, 20);
		add(lblAdvertencia);
		
		listModel = new DefaultListModel<RuralHouse>();
		lstCasas = new JList<RuralHouse>(listModel);
		lstCasas.setCellRenderer(new RuralHouseCellRenderer());
		lstCasas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane = new JScrollPane(lstCasas);
		scrollPane.setBounds(50, 100, 700, 400);
		add(scrollPane);
		
		JButton btnAceptar;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoAceptar = ImageIO.read(new File("res/button-ok.png"));
			btnAceptar = new JButton(new ImageIcon(iconoAceptar));
			btnAceptar.setBorder(BorderFactory.createEmptyBorder());
			btnAceptar.setContentAreaFilled(false);
			btnAceptar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnAceptar = new JButton("Aceptar");
		}
		btnAceptar.setBounds(300, 530, 200, 50);
		btnAceptar.addActionListener(this);
		add(btnAceptar);
		
		JButton btnVolver;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-back.png"));
			btnVolver = new JButton(new ImageIcon(iconoVolver));
			btnVolver.setBorder(BorderFactory.createEmptyBorder());
			btnVolver.setContentAreaFilled(false);
			btnVolver.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnVolver = new JButton("Volver");
			btnVolver.setFont(new Font("Tahoma", Font.ITALIC, 14));
			btnVolver.setForeground(Color.BLUE);
		}
		btnVolver.setBounds(25, 5, 110, 43);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(0);
			}
		});
		add(btnVolver);
	}
	
	@Override
	public boolean init() {
		try {
			// Obtiene las casas rurales del propietario
			Vector<RuralHouse> casas = MainWindow.getBusinessLogic().getRuralHouses((Owner) MainWindow.getUser());
			if (casas.isEmpty()) {
				// Si no hay casas, mensaje de error
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "No dispone de ninguna casa rural en el sistema", "Advertencia", JOptionPane.WARNING_MESSAGE);
				return false;
			} else {
				listModel.clear();
				for (RuralHouse rh : casas)
					listModel.addElement(rh);
				return true;
			}
		} catch (RemoteException e1) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (lstCasas.isSelectionEmpty()) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Seleccione una casa de la lista", "Advertencia", JOptionPane.WARNING_MESSAGE);
		} else {
			int respuesta = JOptionPane.showOptionDialog(MainWindow.getFrame(), 
					"<html>¡CUIDADO! Tenga en cuenta que eliminar una casa conlleva la eliminación de sus ofertas, reservas y comentarios si los hubiera.<br>" +
					"Los clientes que hayan hecho una reserva futura en esta casa serán notificados de su cancelación.<br>" +
					"¿Ha entendido e igualmente desea continuar con la operación?</html>", "Advertencia", JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE, null, null, null);
			if (respuesta == JOptionPane.YES_OPTION) {
				try {
					MainWindow.getBusinessLogic().deleteRuralHouse(lstCasas.getSelectedValue());
					JOptionPane.showMessageDialog(MainWindow.getFrame(), "Casa eliminada con éxito", "Correcto", JOptionPane.INFORMATION_MESSAGE);
					MainWindow.getFrame().setCurrentPage(0);
				} catch (RemoteException e1) {
					JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

}
