package gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXDatePicker;

import configuration.Config;
import domain.Offer;
import domain.RuralHouse;

public class SearchOffersPanel extends ApplicationPage {

	private static final long serialVersionUID = 1L;

	private JComboBox<String> comboBoxCiudad;
	private DefaultComboBoxModel<String> comboBoxModel;
	private JXDatePicker datePickerInicio;
	private JXDatePicker datePickerFin;
	private JButton btnBuscar;
	private JScrollPane scrollPane;
	private JList<Offer> listOfertas;
	private DefaultListModel<Offer> listModel;

	/**
	 * Create the panel.
	 */
	public SearchOffersPanel() {
		super("Ver ofertas por localidad");
		
		JLabel lblCiudad = new JLabel("Ciudad:");
		lblCiudad.setBounds(50, 80, 50, 20);
		add(lblCiudad);
		
		comboBoxModel = new DefaultComboBoxModel<String>();
		comboBoxCiudad = new JComboBox<String>(comboBoxModel);
		comboBoxCiudad.setBounds(100, 80, 175, 20);
		add(comboBoxCiudad);
		
		JLabel lblDiaInicio = new JLabel("Fecha de entrada:");
		lblDiaInicio.setBounds(310, 80, 100, 20);
		add(lblDiaInicio);
		
		JLabel lblDiaFin = new JLabel("Fecha de salida:");
		lblDiaFin.setBounds(540, 80, 80, 20);
		add(lblDiaFin);
		
		datePickerInicio = new JXDatePicker(Config.getLocale());
		datePickerInicio.setBounds(410, 80, 100, 20);
		datePickerInicio.setFormats("d MMM YYYY");
		datePickerInicio.setLinkDay(new Date(), "Hoy es {0,date,d 'de' MMMM 'del' y}");
		datePickerInicio.getEditor().setEditable(false);
		datePickerInicio.getMonthView().setLowerBound(new Date());
		datePickerInicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (datePickerInicio.getDate() != null) {
					// Como mínimo un día después de la fecha de inicio
					Date diaDespues = new Date(datePickerInicio.getDate().getTime() + 86400000);
					datePickerFin.getMonthView().setLowerBound(diaDespues);
					datePickerFin.getMonthView().setFirstDisplayedDay(diaDespues);
				} else {
					datePickerFin.setDate(null);
				}
			}
		});
		add(datePickerInicio);
		
		datePickerFin = new JXDatePicker(Config.getLocale());
		datePickerFin.setBounds(630, 80, 100, 20);
		datePickerFin.setFormats("d MMM YYYY");
		datePickerFin.setLinkDay(new Date(), "Hoy es {0,date,d 'de' MMMM 'del' y}");
		datePickerFin.getEditor().setEditable(false);
		add(datePickerFin);
		
		try {
			// Cargar la imagen del botón
			BufferedImage iconoAceptar = ImageIO.read(new File("res/button-search.png"));
			btnBuscar = new JButton(new ImageIcon(iconoAceptar));
			btnBuscar.setBorder(BorderFactory.createEmptyBorder());
			btnBuscar.setContentAreaFilled(false);
			btnBuscar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnBuscar = new JButton("Buscar");
		}
		btnBuscar.setBounds(300, 120, 200, 50);
		btnBuscar.addActionListener(this);
		add(btnBuscar);
		
		JLabel lblOfertasEncontradas = new JLabel();
		lblOfertasEncontradas.setForeground(new Color(0, 0, 205));
		lblOfertasEncontradas.setHorizontalAlignment(SwingConstants.CENTER);
		lblOfertasEncontradas.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblOfertasEncontradas.setBounds(250, 200, 300, 20);
		lblOfertasEncontradas.setText("Ofertas encontradas:");	
		add(lblOfertasEncontradas);	
		
		listModel = new DefaultListModel<Offer>();
		listOfertas = new JList<Offer>(listModel);
		listOfertas.setCellRenderer(new OfferCellRenderer());
		listOfertas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane = new JScrollPane(listOfertas);
		scrollPane.setBounds(50, 230, 700, 280);
		add(scrollPane);
		
		JButton btnAceptar;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoAceptar = ImageIO.read(new File("res/button-ok.png"));
			btnAceptar = new JButton(new ImageIcon(iconoAceptar));
			btnAceptar.setBorder(BorderFactory.createEmptyBorder());
			btnAceptar.setContentAreaFilled(false);
			btnAceptar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnAceptar = new JButton("Aceptar");
		}
		btnAceptar.setBounds(300, 530, 200, 50);
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (listOfertas.isSelectionEmpty()) {
					if (listModel.size() > 0)
						JOptionPane.showMessageDialog(MainWindow.getFrame(), "Seleccione una oferta de la lista", "Advertencia", JOptionPane.WARNING_MESSAGE);
					else
						JOptionPane.showMessageDialog(MainWindow.getFrame(), "Realice una búsqueda que devuelva ofertas válidas", "Advertencia", JOptionPane.WARNING_MESSAGE);
				} else {
					((MakeBookingPanel) MainWindow.getFrame().getPage(8)).init(listOfertas.getSelectedValue());
					MainWindow.getFrame().setCurrentPage(8);
				}
			}
		});
		add(btnAceptar);
		
		JButton btnVolver;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-back.png"));
			btnVolver = new JButton(new ImageIcon(iconoVolver));
			btnVolver.setBorder(BorderFactory.createEmptyBorder());
			btnVolver.setContentAreaFilled(false);
			btnVolver.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnVolver = new JButton("Volver");
			btnVolver.setFont(new Font("Tahoma", Font.ITALIC, 14));
			btnVolver.setForeground(Color.BLUE);
		}
		btnVolver.setBounds(25, 5, 110, 43);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(0);
			}
		});
		add(btnVolver);
	}

	@Override
	public boolean init() {
		return true;
	}
	
	/**
	 * Inicializa la página. Llamar sólo desde StartPanel.
	 */
	public void clear() {
		// Reiniciar campos
		comboBoxModel.removeAllElements();
		datePickerInicio.setDate(null);
		datePickerFin.setDate(null);
		listModel.clear();
		// Cargar la lista de ciudades
		try {
			Vector<String> ciudades = MainWindow.getBusinessLogic().getAllCities();
			if (ciudades.isEmpty()) {
				// No hay casas rurales
				comboBoxCiudad.setEnabled(false);
				datePickerInicio.setEnabled(false);
				datePickerFin.setEnabled(false);
				btnBuscar.setVisible(false);
			} else {
				for (String s : ciudades)
					comboBoxModel.addElement(s);
				comboBoxCiudad.setEnabled(true);
				datePickerInicio.setEnabled(true);
				datePickerFin.setEnabled(true);
				btnBuscar.setVisible(true);
			}
		} catch (RemoteException e1) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (datePickerInicio.getDate() == null) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Seleccione una fecha de entrada", "Advertencia", JOptionPane.WARNING_MESSAGE);
		} else if (datePickerFin.getDate() == null) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Seleccione una fecha de salida", "Advertencia", JOptionPane.WARNING_MESSAGE);
		} else {
			listModel.clear();
			try {
				Vector<RuralHouse> casas = MainWindow.getBusinessLogic().getRuralHouses((String) comboBoxModel.getSelectedItem());
				// Buscar las ofertas disponibles en las fechas dadas en cada casa de la ciudad
				for (RuralHouse casa : casas)
					for (Offer oferta : casa.findOffers(datePickerInicio.getDate(), datePickerFin.getDate()))
					 	listModel.addElement(oferta);
				if (listModel.isEmpty()) {
					// Añadir todas las ofertas disponibles en cada casa de la ciudad
					for (RuralHouse casa : casas)
						for (Offer oferta : casa.getAvailableOffers())
							listModel.addElement(oferta);
					if (listModel.isEmpty()) {
						JOptionPane.showMessageDialog(MainWindow.getFrame(), "No hay ninguna oferta disponible en esa localidad.", "Advertencia", JOptionPane.WARNING_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(MainWindow.getFrame(), "No hay ofertas disponibles para esa fecha. Se muestran todas las ofertas disponibles en esa localidad.", "Advertencia", JOptionPane.WARNING_MESSAGE);
					}
				}
			} catch (RemoteException e1) {
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

}
