package gui;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.Naming;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import businessLogic.ApplicationFacadeInterface;
import configuration.Config;
import domain.User;

public class MainWindow extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private static final boolean isLocal = false;
	private static ApplicationFacadeInterface facadeInterface;
	private static MainWindow window;
	private static User user = null;
	
	private final JPanel contentPane = new JPanel(null);
	private final JPanel headerPane = new HeaderPanel();
	private final ApplicationPage paginas[] = { new StartPanel(), new AddRuralHousePanel(), new RemoveRuralHousePanel(), new RuralHouseManagementPanel(), new CreateOfferPanel(),
			new RemoveOfferPanel(), new BookManagementPanel(), new SearchOffersPanel(), new MakeBookingPanel(), new CancelBookingPanel(), new MakeCommentPanel() };
	private int paginaActual = 0;
	
	/**
	 * This is the default constructor
	 */
	public MainWindow() {
		contentPane.setPreferredSize(new Dimension(800, 700));
		setContentPane(contentPane);
		
		// Cargar páginas
		contentPane.add(headerPane);
		for (int i = 0; i < paginas.length; ++i) {
			contentPane.add(paginas[i]);
			paginas[i].setVisible(false);
		}
		paginas[0].setVisible(true);
		pack();
		
		// Ajustes de la ventana
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		setTitle("Gestión de casas rurales");
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				cerrarVentana();
			}
		});
	}
	
	/**
	 * Devuelve la página correspondiente al índice pasado como parámetro
	 * @param index
	 * 		0 = Inicio
	 * 		1 = Añadir casa rural
	 * 		2 = Eliminar casa rural
	 * 		3 = Gestionar casas
	 * 		4 = Crear oferta
	 * 		5 = Eliminar oferta
	 * 		6 = Gestionar reservas
	 * 		7 = Buscar ofertas
	 * 		8 = Reservar casa rural
	 * 		9 = Cancelar una reserva
	 * 		10 = Hacer un comentario
	 * @return un objeto ApplicationPage de la página solicitada
	 */
	protected ApplicationPage getPage(int index) {
		if (index >= 0 && index < paginas.length)
			return paginas[index];
		else
			return null;
	}
	
	/**
	 * Devuelve el índice correspondiente a la página actual
	 * @return un entero (comprobar lista de getPage(int))
	 */
	protected int getCurrentPage() {
		return paginaActual;
	}
	
	/**
	 * Pasa a la página indicada. Si la precarga de la nueva página falla, se mantiene la actual.
	 * @param index
	 * 			Un valor entero (comprobar lista de getPage(int))
	 */
	protected void setCurrentPage(int index) {
		if (index >= 0 && index < paginas.length && paginas[index].init()) {
			paginas[paginaActual].setVisible(false);
			paginaActual = index;
			paginas[paginaActual].setVisible(true);
		}
	}
	
	/**
	 * Cerrar la base de datos (local), eliminar la ventana y terminar la aplicación
	 */
	private void cerrarVentana() {
		if (isLocal) {
			// Descomentar para funcionar con fachada local
			/*try {
				facadeInterface.closeDatabase();
			} catch (RemoteException e) {
				JOptionPane.showMessageDialog(window, "Error inesperado al consultar el servidor remoto: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}*/
		}
		this.dispose();
		System.exit(0);
	}
	
	/**
	 * Returns the business logic
	 * @return an application facade interface
	 */
	public static ApplicationFacadeInterface getBusinessLogic() {
		return facadeInterface;
	}
	
	/**
	 * Returns a reference to this frame
	 * @return the MainWindow object
	 */
	public static MainWindow getFrame() {
		return window;
	}
	
	/**
	 * Returns a reference to the logged user
	 * @return a User object
	 */
	public static User getUser() {
		return user;
	}
	
	/**
	 * Sets the identity of the current user
	 * @param user - the user to set
	 */
	public static void setUser(User user) {
		MainWindow.user = user;
	}

	/**
	 * Application entry point
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			Config.setDecimalSeparator();
			
			if (isLocal) {
				// Descomentar la línea de abajo para funcionar con fachada local
				//facadeInterface = new FacadeImplementation();
			} else {
				// RMI service name
				String service = "//" + Config.getServerRMI() + ":" + Config.getPortRMI() + "/" + Config.getServiceRMI();
				facadeInterface = (ApplicationFacadeInterface) Naming.lookup(service);
			}
		} catch (Exception e) {
			System.err.println("No se puede inicializar el sistema: " + e.getMessage());
			System.exit(0);
		}

		window = new MainWindow();
		window.setVisible(true);
	}
	
}
