package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public abstract class ApplicationPage extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructora: establece absolute layout, el tamaño de página y el título.
	 */
	public ApplicationPage(String title) {
		super(null);
		setBounds(0, 100, 800, 600);
		
		JLabel lblTitulo = new JLabel(title);
		lblTitulo.setForeground(new Color(0, 0, 128));
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblTitulo.setBounds(200, 20, 400, 20);
		add(lblTitulo);
	}
	
	/**
	 * Inicializar la página antes de mostrarla: vacía los campos, rellena listas, etc.
	 * @return true si la precarga se ha realizado con éxito
	 */
	public abstract boolean init();
	
}
