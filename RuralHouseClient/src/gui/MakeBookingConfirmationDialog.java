package gui;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTextField;

import configuration.Config;

import domain.Book;

public class MakeBookingConfirmationDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	/**
	 * Create the dialog.
	 */
	public MakeBookingConfirmationDialog(Book book) {
		super(MainWindow.getFrame(), true);
		
		setSize(450, 400);
		setResizable(false);
		setLocationRelativeTo(MainWindow.getFrame());
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setTitle("Confirmación de la reserva de la casa rural");
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				cerrarDialogo();
			}
		});
		
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		
		JLabel lblTitulo = new JLabel("Confirmación de la reserva");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setFont(new Font("Verdana", Font.BOLD, 16));
		lblTitulo.setBounds(75, 15, 300, 20);
		contentPanel.add(lblTitulo);

		JLabel lblAviso = new JLabel("<html><p style='text-align:center;'>¡Guarde su código de reserva a buen recaudo!<br>" +
				"Lo necesitará para identificarse en la casa.</p></html>");
		lblAviso.setHorizontalAlignment(SwingConstants.CENTER);
		lblAviso.setBounds(25, 45, 400, 30);
		contentPanel.add(lblAviso);
		
		JLabel lblPass = new JLabel("Código de reserva:");
		lblPass.setBounds(30, 85, 110, 16);
		contentPanel.add(lblPass);
		
		JTextField nBook = new JTextField(book.getBookCode());
		nBook.setBounds(150, 83, 240, 20);
		nBook.setEditable(false);
		contentPanel.add(nBook);
		
		if (book.getStatus() == Book.PENDING) {
			// Ingresar un 30% en tres días
			JLabel lblEmail = new JLabel("Nº de cuenta ingreso:");
			lblEmail.setBounds(30, 115, 110, 16);
			contentPanel.add(lblEmail);
			
			JTextField nCuenta = new JTextField(book.getOffer().getRuralHouse().getOwner().getBankAccount());
			nCuenta.setBounds(150, 113, 240, 20);
			nCuenta.setEditable(false);
			contentPanel.add(nCuenta);
			
			JLabel lblDeposit = new JLabel("<html><p style='text-align: center;'>Debe depositar el 30% de la cantidad total de la reserva en los próximos tres días, o será desestimada. " +
					"Indique el código de reserva en la transferencia bancaria.</p></html>");
			lblDeposit.setHorizontalAlignment(SwingConstants.CENTER);
			lblDeposit.setBounds(15, 140, 420, 38);
			contentPanel.add(lblDeposit);
			
			JLabel lblTotal = new JLabel("Total:");
			lblTotal.setBounds(30, 190, 110, 16);
			contentPanel.add(lblTotal);
	
			JTextField total = new JTextField(Config.getDecimalFormat().format(book.getOffer().getPrice()) + " €");
			total.setBounds(150, 188, 120, 20);
			total.setEditable(false);
			contentPanel.add(total);
			
			JLabel lblCantDepo = new JLabel("Cantidad a depositar:");
			lblCantDepo.setBounds(30, 220, 110, 16);
			contentPanel.add(lblCantDepo);
			
			JTextField cantDepo = new JTextField(Config.getDecimalFormat().format(book.getOffer().getPrice() * 0.3f) + " €");
		    cantDepo.setBounds(150, 218, 120, 20);
		    cantDepo.setEditable(false);
		    contentPanel.add(cantDepo);
		    
		    JLabel lblResto = new JLabel("<html><p style='text-align: center;'>Puede ingresar en cualquier momento la cantidad restante, o abonarla el día de la entrada en la recepción de la propia casa: " +
		    		Config.getDecimalFormat().format(book.getOffer().getPrice() * 0.7f) + " €</p></html>");
		    lblResto.setHorizontalAlignment(SwingConstants.CENTER);
		    lblResto.setBounds(15, 245, 420, 30);
			contentPanel.add(lblResto);
			
			JLabel lblCancelacion = new JLabel("<html><p style='text-align: center;'>Puede cancelar su reserva hasta un día antes de la fecha de inicio.<br>" +
					"Sin embargo, si cancela con menos de una semana de antelación perderá el depósito.</p></html>");
			lblCancelacion.setBounds(15, 282, 420, 38);
			contentPanel.add(lblCancelacion);
		} else {
			// Pagar al llegar a la casa
			JLabel lblAbono = new JLabel("<html><p style='text-align: center;'>Deberá abonar la cantidad abajo indicada el día de la entrada, <br>en la recepción de la propia casa.</p></html>");
			lblAbono.setHorizontalAlignment(SwingConstants.CENTER);
			lblAbono.setBounds(20, 130, 400, 38);
			contentPanel.add(lblAbono);
			
			JLabel lblTotal = new JLabel("Total:");
			lblTotal.setBounds(30, 190, 110, 16);
			contentPanel.add(lblTotal);
	
			JTextField total = new JTextField(Config.getDecimalFormat().format(book.getOffer().getPrice()) + " €");
			total.setBounds(150, 188, 120, 20);
			total.setEditable(false);
			contentPanel.add(total);
			
			JLabel lblCancelacion = new JLabel("<html>Puede cancelar su reserva hasta un día antes de la fecha de inicio.</html>");
			lblCancelacion.setHorizontalAlignment(SwingConstants.CENTER);
			lblCancelacion.setBounds(15, 275, 420, 20);
			contentPanel.add(lblCancelacion);
		}
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(175, 325, 100, 26);
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				cerrarDialogo();
			}
		});
		contentPanel.add(btnAceptar);
	}
	
	/**
	 * Volver a la página principal.
	 */
	public void cerrarDialogo() {
		dispose();
		MainWindow.getFrame().setCurrentPage(0);
	}
	
}
