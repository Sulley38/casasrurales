package gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

import domain.Offer;
import domain.Owner;
import domain.RuralHouse;

public class RemoveOfferPanel extends ApplicationPage {

	private static final long serialVersionUID = 1L;
	
	private JList<RuralHouse> lstCasas;
	private DefaultListModel<RuralHouse> listModel;
	private JScrollPane scrollPane;
	private JComboBox<Offer> comboBoxOfertas;
	private DefaultComboBoxModel<Offer> comboBoxModel;
	private JButton btnAceptar;

	/**
	 * Create the panel.
	 */
	public RemoveOfferPanel() {
		super("Eliminar una oferta");		
		
		JLabel lblListado = new JLabel("Seleccione la casa rural de la que desea eliminar alguna oferta:");
		lblListado.setBounds(50, 65, 350, 14);
		add(lblListado);
		
		listModel = new DefaultListModel<RuralHouse>();
		lstCasas = new JList<RuralHouse>(listModel);
		lstCasas.setCellRenderer(new RuralHouseCellRenderer());
		lstCasas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstCasas.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (!lstCasas.isSelectionEmpty()) {
					// Rellena el ComboBox con las ofertas de la casa seleccionada
					comboBoxModel.removeAllElements();
					Vector<Offer> ofertas = lstCasas.getSelectedValue().getOffers();
					if (ofertas.isEmpty()) {
						comboBoxOfertas.setEnabled(false);
						btnAceptar.setVisible(false);
					} else {
						comboBoxOfertas.setEnabled(true);
						btnAceptar.setVisible(true);
						for (Offer o : ofertas)
							comboBoxModel.addElement(o);
					}
				}
			}
		});
		scrollPane = new JScrollPane(lstCasas);
		scrollPane.setBounds(50, 90, 700, 380);
		add(scrollPane);

		JLabel lblOfertas = new JLabel("Ofertas para la casa seleccionada:");
		lblOfertas.setBounds(100, 490, 170, 24);
		add(lblOfertas);
		
		comboBoxModel = new DefaultComboBoxModel<Offer>();
		comboBoxOfertas = new JComboBox<Offer>(comboBoxModel);
		comboBoxOfertas.setBounds(280, 490, 420, 24);
		add(comboBoxOfertas);
		
		try {
			// Cargar la imagen del botón
			BufferedImage iconoAceptar = ImageIO.read(new File("res/button-ok.png"));
			btnAceptar = new JButton(new ImageIcon(iconoAceptar));
			btnAceptar.setBorder(BorderFactory.createEmptyBorder());
			btnAceptar.setContentAreaFilled(false);
			btnAceptar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnAceptar = new JButton("Aceptar");
		}
		btnAceptar.setBounds(300, 530, 200, 50);
		btnAceptar.addActionListener(this);
		add(btnAceptar);
		
		JButton btnVolver;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-back.png"));
			btnVolver = new JButton(new ImageIcon(iconoVolver));
			btnVolver.setBorder(BorderFactory.createEmptyBorder());
			btnVolver.setContentAreaFilled(false);
			btnVolver.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnVolver = new JButton("Volver");
			btnVolver.setFont(new Font("Tahoma", Font.ITALIC, 14));
			btnVolver.setForeground(Color.BLUE);
		}
		btnVolver.setBounds(25, 5, 110, 43);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(0);
			}
		});
		add(btnVolver);
	}

	@Override
	public boolean init() {
		try {
			// Obtiene las casas rurales del propietario
			Vector<RuralHouse> casas = MainWindow.getBusinessLogic().getRuralHouses((Owner) MainWindow.getUser());
			if (casas.isEmpty()) {
				// Si no hay casas, mensaje de error
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "No dispone de ninguna casa rural en el sistema", "Advertencia", JOptionPane.WARNING_MESSAGE);
				return false;
			} else {
				listModel.clear();
				for (RuralHouse rh : casas)
					listModel.addElement(rh);
				// Reiniciar el comboBox
				comboBoxModel.removeAllElements();
				comboBoxOfertas.setEnabled(false);
				btnAceptar.setVisible(false);
				return true;
			}
		} catch (RemoteException e1) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		int respuesta = JOptionPane.showOptionDialog(MainWindow.getFrame(), 
				"<html>¡CUIDADO! Tenga en cuenta que eliminar una oferta conlleva la eliminación de su reserva si la hubiera.<br>" +
				"Si un cliente ha reservado esta oferta será notificado de su cancelación.<br>" +
				"¿Ha entendido e igualmente desea continuar con la operación?</html>", "Advertencia", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, null, null);
		if (respuesta == JOptionPane.YES_OPTION) {
			try {
				MainWindow.getBusinessLogic().removeOffer((Offer) comboBoxOfertas.getSelectedItem());
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Oferta eliminada con éxito", "Correcto", JOptionPane.INFORMATION_MESSAGE);
				MainWindow.getFrame().setCurrentPage(0);
			} catch (RemoteException e1) {
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

}
