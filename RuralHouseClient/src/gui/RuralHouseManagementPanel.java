package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.JComboBox;
import javax.swing.filechooser.FileNameExtensionFilter;

import domain.Comment;
import domain.Owner;
import domain.RuralHouse;

public class RuralHouseManagementPanel extends ApplicationPage {

	private static final long serialVersionUID = 1L;
	
	private JComboBox<RuralHouse> comboCasas;
	private DefaultComboBoxModel<RuralHouse> comboBoxModel;
	private JTextArea txtDescripcion;
	private JScrollPane scrollDescripcion;
	private JSpinner spnHabitaciones;
	private JSpinner spnCocinas;
	private JSpinner spnBanos;
	private JSpinner spnSalas;
	private JSpinner spnAparcamientos;
	private JList<ImageIcon> listImages;
	private DefaultListModel<ImageIcon> imageListModel;
	private JScrollPane scrollImages;
	private JList<Comment> listComments;
	private DefaultListModel<Comment> commentListModel;
	private JScrollPane scrollComments;
	
	/**
	 * Create the panel.
	 */
	public RuralHouseManagementPanel() {
		super("Gestionar casas");
		
		JLabel lblSeleccione = new JLabel("Seleccione una casa:");
		lblSeleccione.setBounds(60, 60, 120, 15);
		add(lblSeleccione);
		
		comboBoxModel = new DefaultComboBoxModel<RuralHouse>();
		comboCasas = new JComboBox<RuralHouse>(comboBoxModel);
		comboCasas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (comboCasas.isShowing()) {
					// Rellenar los campos con los datos correspondientes a la casa seleccionada
					RuralHouse casaElegida = (RuralHouse) comboBoxModel.getSelectedItem();
					txtDescripcion.setText(casaElegida.getDescription());
					spnHabitaciones.setValue(casaElegida.getRooms());
					spnCocinas.setValue(casaElegida.getKitchens());
					spnBanos.setValue(casaElegida.getBaths());
					spnSalas.setValue(casaElegida.getLivings());
					spnAparcamientos.setValue(casaElegida.getParkings());
					imageListModel.clear();
					for (byte[] img : casaElegida.getPictures())
						imageListModel.addElement(new ImageIcon(img));
					commentListModel.clear();
					for (Comment c : casaElegida.getComments())
						commentListModel.addElement(c);
				}
			}
		});
		comboCasas.setBounds(180, 55, 520, 25);
		add(comboCasas);
		
		JPanel panelEdicion = new JPanel(null);
        panelEdicion.setBorder(BorderFactory.createTitledBorder("Editar características:"));
        panelEdicion.setBounds(40, 90, 720, 370);
        add(panelEdicion);
        
		JLabel lblDescripcion = new JLabel("Descripción:");
		lblDescripcion.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDescripcion.setBounds(10, 20, 80, 22);
		panelEdicion.add(lblDescripcion);
		
		txtDescripcion = new JTextArea(new DefaultStyledDocument() {
			private static final long serialVersionUID = 1L;
			@Override
		    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
		        if ((getLength() + str.length()) <= 160) {
		            super.insertString(offs, str, a);
		        } else {
		            Toolkit.getDefaultToolkit().beep();
		        }
		    }
		});
		txtDescripcion.setToolTipText("Este campo es opcional. Máximo 160 caracteres.");
		scrollDescripcion = new JScrollPane(txtDescripcion);
		scrollDescripcion.setBounds(100, 20, 560, 66);
		panelEdicion.add(scrollDescripcion);
		
		JLabel lblHabitaciones = new JLabel("Habitaciones:");
		lblHabitaciones.setHorizontalAlignment(SwingConstants.RIGHT);
		lblHabitaciones.setBounds(10, 100, 80, 22);
		panelEdicion.add(lblHabitaciones);
		
		spnHabitaciones = new JSpinner(new SpinnerNumberModel(3, 3, 50, 1));
		spnHabitaciones.setBounds(100, 100, 40, 22);
		panelEdicion.add(spnHabitaciones);
		
		JLabel lblCocinas = new JLabel("Cocinas:");
		lblCocinas.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCocinas.setBounds(150, 100, 60, 22);
		panelEdicion.add(lblCocinas);
		
		spnCocinas = new JSpinner(new SpinnerNumberModel(1, 1, 50, 1));
		spnCocinas.setBounds(220, 100, 40, 22);
		panelEdicion.add(spnCocinas);
		
		JLabel lblBanos = new JLabel("Baños:");
		lblBanos.setHorizontalAlignment(SwingConstants.RIGHT);
		lblBanos.setBounds(270, 100, 60, 22);
		panelEdicion.add(lblBanos);
		
		spnBanos = new JSpinner(new SpinnerNumberModel(2, 2, 50, 1));
		spnBanos.setBounds(340, 100, 40, 22);
		panelEdicion.add(spnBanos);
		
		JLabel lblSalas = new JLabel("Salas de estar:");
		lblSalas.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSalas.setBounds(390, 100, 80, 22);
		panelEdicion.add(lblSalas);
		
		spnSalas = new JSpinner(new SpinnerNumberModel(0, 0, 50, 1));
		spnSalas.setBounds(480, 100, 40, 22);
		panelEdicion.add(spnSalas);
		
		JLabel lblAparcamientos = new JLabel("Aparcamientos:");
		lblAparcamientos.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAparcamientos.setBounds(530, 100, 80, 22);
		panelEdicion.add(lblAparcamientos);
		
		spnAparcamientos = new JSpinner(new SpinnerNumberModel(0, 0, 50, 1));
		spnAparcamientos.setBounds(620, 100, 40, 22);
		panelEdicion.add(spnAparcamientos);
		
		imageListModel = new DefaultListModel<ImageIcon>();
		listImages = new JList<ImageIcon>(imageListModel);
		listImages.setCellRenderer(new DefaultListCellRenderer() {
			private static final long serialVersionUID = 1L;
			@Override
			public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				setHorizontalAlignment(SwingConstants.CENTER);
				setVerticalAlignment(SwingConstants.CENTER);
				Image icono = ((ImageIcon) value).getImage().getScaledInstance(-1, 100, Image.SCALE_FAST);
				setIcon(new ImageIcon(icono));
				setPreferredSize(new Dimension(getIcon().getIconWidth() + 10, 110));
				return this;
			}
		});
		listImages.setLayoutOrientation(JList.VERTICAL_WRAP);
		listImages.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listImages.setVisibleRowCount(-1);
		scrollImages = new JScrollPane(listImages);
		scrollImages.setBounds(15, 140, 650, 130);
		panelEdicion.add(scrollImages);
		
		JButton btnAnadir;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-add.png"));
			btnAnadir = new JButton(new ImageIcon(iconoVolver));
			btnAnadir.setBorder(BorderFactory.createEmptyBorder());
			btnAnadir.setContentAreaFilled(false);
			btnAnadir.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnAnadir = new JButton("+");
			btnAnadir.setFont(new Font("Tahoma", Font.BOLD, 14));
			btnAnadir.setForeground(Color.GREEN);
		}
		btnAnadir.setBounds(670, 140, 32, 32);
		btnAnadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Mostrar diálogo de Abrir archivo
				FileNameExtensionFilter filtro = new FileNameExtensionFilter("Imágenes (*.bmp, *.gif, *.jpg, *.png)", "bmp", "gif", "jpg", "png");
				JFileChooser fc = new JFileChooser();
				fc.setFileFilter(filtro);
				fc.setMultiSelectionEnabled(true);
				int respuesta = fc.showOpenDialog(MainWindow.getFrame());
				if (respuesta == JFileChooser.APPROVE_OPTION) {
					// Añadir las nuevas imágenes
					for (File f : fc.getSelectedFiles()) {
						try {
							BufferedImage imgOriginal = ImageIO.read(f), imgRedimensionada = new BufferedImage(400, Math.round(imgOriginal.getHeight() * 400 / imgOriginal.getWidth()), BufferedImage.TYPE_INT_RGB);
							Graphics2D g = imgRedimensionada.createGraphics();
							g.drawImage(imgOriginal, 0, 0, 400, Math.round(imgOriginal.getHeight() * 400 / imgOriginal.getWidth()), null);
							g.dispose();
							imageListModel.addElement(new ImageIcon(imgRedimensionada));
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error al cargar la imagen: " + e1.getMessage(), "Error", JOptionPane.WARNING_MESSAGE);
						}
					}
				}
			}
		});
		panelEdicion.add(btnAnadir);
		
		JButton btnQuitar;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-remove.png"));
			btnQuitar = new JButton(new ImageIcon(iconoVolver));
			btnQuitar.setBorder(BorderFactory.createEmptyBorder());
			btnQuitar.setContentAreaFilled(false);
			btnQuitar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnQuitar = new JButton("-");
			btnQuitar.setFont(new Font("Tahoma", Font.BOLD, 14));
			btnQuitar.setForeground(Color.RED);
		}
		btnQuitar.setBounds(670, 180, 32, 32);
		btnQuitar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Eliminar la imagen seleccionada
				if (!listImages.isSelectionEmpty())
					imageListModel.removeElementAt(listImages.getSelectedIndex());
			}
		});
		panelEdicion.add(btnQuitar);
		
		JLabel lblNota = new JLabel("Las imágenes son redimensionadas de forma automática. La primera se usará como portada de la casa.");
		lblNota.setHorizontalAlignment(SwingConstants.CENTER);
		lblNota.setBounds(60, 280, 600, 15);
		panelEdicion.add(lblNota);

		JButton btnGuardar;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoGuardar = ImageIO.read(new File("res/button-save.png"));
			btnGuardar = new JButton(new ImageIcon(iconoGuardar));
			btnGuardar.setBorder(BorderFactory.createEmptyBorder());
			btnGuardar.setContentAreaFilled(false);
			btnGuardar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnGuardar = new JButton("Guardar");
		}
		btnGuardar.setBounds(260, 310, 200, 50);
		btnGuardar.addActionListener(this);
		panelEdicion.add(btnGuardar);
		
		JPanel panelModeracion = new JPanel(null);
		panelModeracion.setBorder(BorderFactory.createTitledBorder("Moderar comentarios:"));
		panelModeracion.setBounds(40, 460, 720, 130);
        add(panelModeracion);
		
		commentListModel = new DefaultListModel<Comment>();
		listComments = new JList<Comment>(commentListModel);
		listComments.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollComments = new JScrollPane(listComments);
		scrollComments.setBounds(15, 20, 650, 100);
		panelModeracion.add(scrollComments);
		
		JButton btnPapelera;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-trash.png"));
			btnPapelera = new JButton(new ImageIcon(iconoVolver));
			btnPapelera.setBorder(BorderFactory.createEmptyBorder());
			btnPapelera.setContentAreaFilled(false);
			btnPapelera.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnPapelera = new JButton("X");
			btnPapelera.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		btnPapelera.setBounds(670, 50, 32, 32);
		btnPapelera.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Eliminar el comentario seleccionado
				if (!listComments.isSelectionEmpty()) {
					try {
						int respuesta = JOptionPane.showOptionDialog(MainWindow.getFrame(), "<html>Esta acción no se puede deshacer.<br>" +
								"¿Está seguro de que desea eliminar el comentario seleccionado?</html>", "Advertencia", JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE, null, null, null);
						if (respuesta == JOptionPane.YES_OPTION) {
							MainWindow.getBusinessLogic().deleteComment(listComments.getSelectedValue());
							commentListModel.removeElementAt(listComments.getSelectedIndex());
							JOptionPane.showMessageDialog(MainWindow.getFrame(), "Comentario eliminado con éxito", "Correcto", JOptionPane.INFORMATION_MESSAGE);
						}
					} catch (RemoteException e1) {
						JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		panelModeracion.add(btnPapelera);
		
		JButton btnVolver;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-back.png"));
			btnVolver = new JButton(new ImageIcon(iconoVolver));
			btnVolver.setBorder(BorderFactory.createEmptyBorder());
			btnVolver.setContentAreaFilled(false);
			btnVolver.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnVolver = new JButton("Volver");
			btnVolver.setFont(new Font("Tahoma", Font.ITALIC, 14));
			btnVolver.setForeground(Color.BLUE);
		}
		btnVolver.setBounds(25, 5, 110, 43);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(0);
			}
		});
		add(btnVolver);
	}

	@Override
	public boolean init() {
		try {
			// Obtiene las casas rurales del propietario
			Vector<RuralHouse> casas = MainWindow.getBusinessLogic().getRuralHouses((Owner) MainWindow.getUser());
			if (casas.isEmpty()) {
				// Si no hay casas, mensaje de error
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "No dispone de ninguna casa rural en el sistema", "Advertencia", JOptionPane.WARNING_MESSAGE);
				return false;
			} else {
				comboBoxModel.removeAllElements();
				for (RuralHouse rh : casas)
					comboBoxModel.addElement(rh);
				// Cargar campos con los datos de la casa seleccionada
				RuralHouse casaElegida = (RuralHouse) comboBoxModel.getSelectedItem();
				txtDescripcion.setText(casaElegida.getDescription());
				spnHabitaciones.setValue(casaElegida.getRooms());
				spnCocinas.setValue(casaElegida.getKitchens());
				spnBanos.setValue(casaElegida.getBaths());
				spnSalas.setValue(casaElegida.getLivings());
				spnAparcamientos.setValue(casaElegida.getParkings());
				imageListModel.clear();
				for (byte[] img : casaElegida.getPictures())
					imageListModel.addElement(new ImageIcon(img));
				commentListModel.clear();
				for (Comment c : casaElegida.getComments())
					commentListModel.addElement(c);
				return true;
			}
		} catch (RemoteException e1) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		try {
			// Convertir imágenes
			Vector<byte[]> imagenesByte = new Vector<byte[]>();
			for (int i = 0; i < imageListModel.size(); i++) {
				try {
					// Conversión de ImageIcon a BufferedImage
					ImageIcon icono = imageListModel.get(i);
					BufferedImage img = new BufferedImage(icono.getIconWidth(), icono.getIconHeight(), BufferedImage.TYPE_INT_RGB);
					Graphics g = img.createGraphics();
					icono.paintIcon(null, g, 0, 0);
					g.dispose();
					// Conversión de BufferedImage a byte[]
					ByteArrayOutputStream salidaBytes = new ByteArrayOutputStream();
					ImageIO.write(img, "jpg", salidaBytes);
					salidaBytes.flush();
					imagenesByte.add(salidaBytes.toByteArray());
					salidaBytes.close();
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error al cargar una imagen de la casa: " + e1.getMessage(), "Error", JOptionPane.WARNING_MESSAGE);
				}
			}
			// Actualizar objeto remoto
			RuralHouse casaElegida = (RuralHouse) comboBoxModel.getSelectedItem();
			MainWindow.getBusinessLogic().updateRuralHouse(casaElegida, txtDescripcion.getText(), (Integer) spnHabitaciones.getValue(),
					(Integer) spnCocinas.getValue(), (Integer) spnBanos.getValue(), (Integer) spnSalas.getValue(), (Integer) spnAparcamientos.getValue(), imagenesByte );
			// Actualizar objeto local
			casaElegida.setDescription(txtDescripcion.getText());
			casaElegida.setRooms((Integer) spnHabitaciones.getValue());
			casaElegida.setKitchens((Integer) spnCocinas.getValue());
			casaElegida.setBaths((Integer) spnBanos.getValue());
			casaElegida.setLivings((Integer) spnSalas.getValue());
			casaElegida.setParkings((Integer) spnAparcamientos.getValue());
			casaElegida.setPictures(imagenesByte);
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Información de la casa actualizada", "Correcto", JOptionPane.INFORMATION_MESSAGE);
		} catch (RemoteException e1) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
}
