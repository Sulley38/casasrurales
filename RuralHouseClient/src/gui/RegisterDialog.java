package gui;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.regex.Pattern;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ButtonGroup;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;

public class RegisterDialog extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	private final JPanel contentPanel = new JPanel();
	private JLabel lblAccount;
	private JTextField txtEmail;
	private JTextField txtPhone;
	private JTextField txtAccount;
	private JPasswordField pwdPass;
	private JPasswordField pwdPass2;
	private JRadioButton rdbtnCliente;
	private JRadioButton rdbtnPropietario;

	/**
	 * Create the dialog.
	 */
	public RegisterDialog() {
		super(MainWindow.getFrame(), true);
		
		setSize(400, 310);
		setResizable(false);
		setLocationRelativeTo(MainWindow.getFrame());
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle("Registro de nuevo usuario");
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		
		JLabel lblTitulo = new JLabel("Formulario de registro");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setFont(new Font("Verdana", Font.BOLD, 16));
		lblTitulo.setBounds(50, 15, 300, 20);
		contentPanel.add(lblTitulo);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(30, 60, 120, 16);
		contentPanel.add(lblEmail);
		
		JLabel lblPass = new JLabel("Contraseña:");
		lblPass.setBounds(30, 90, 120, 16);
		contentPanel.add(lblPass);
		
		JLabel lblPass2 = new JLabel("Confirmar contraseña:");
		lblPass2.setBounds(30, 120, 120, 16);
		contentPanel.add(lblPass2);
		
		JLabel lblTelephone = new JLabel("Teléfono:");
		lblTelephone.setBounds(30, 150, 120, 16);
		contentPanel.add(lblTelephone);
		
		JLabel lblTipo = new JLabel("Tipo de cuenta:");
		lblTipo.setBounds(30, 180, 120, 16);
		contentPanel.add(lblTipo);
		
		lblAccount = new JLabel("Cuenta bancaria:");
		lblAccount.setBounds(30, 210, 120, 16);
		lblAccount.setVisible(false);
		contentPanel.add(lblAccount);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(160, 58, 190, 20);
		txtEmail.setToolTipText("usuario@dominio");
		txtEmail.addActionListener(this);
		contentPanel.add(txtEmail);
		
		pwdPass = new JPasswordField();
		pwdPass.setBounds(160, 88, 190, 20);
		pwdPass.addActionListener(this);
		contentPanel.add(pwdPass);
		
		pwdPass2 = new JPasswordField();
		pwdPass2.setBounds(160, 118, 190, 20);
		pwdPass2.addActionListener(this);
		contentPanel.add(pwdPass2);
		
		txtPhone = new JTextField();
		txtPhone.setBounds(160, 148, 190, 20);
		txtPhone.setToolTipText("Se espera un valor numérico de 9 dígitos");
		txtPhone.addActionListener(this);
		contentPanel.add(txtPhone);
		
		rdbtnCliente = new JRadioButton("Cliente");
		rdbtnCliente.setBounds(160, 177, 80, 23);
		rdbtnCliente.setSelected(true);
		rdbtnCliente.setToolTipText("Seleccione esta opción para crear una cuenta estándar de cliente");
		rdbtnCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblAccount.setVisible(false);
				txtAccount.setVisible(false);
			}
		});
		contentPanel.add(rdbtnCliente);
		
		rdbtnPropietario = new JRadioButton("Propietario");
		rdbtnPropietario.setBounds(250, 177, 80, 23);
		rdbtnPropietario.setToolTipText("Seleccione esta opción si es usted el propietario de una o más casas rurales");
		rdbtnPropietario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblAccount.setVisible(true);
				txtAccount.setVisible(true);
			}
		});
		contentPanel.add(rdbtnPropietario);
		
		ButtonGroup rdbtnGroup = new ButtonGroup();
		rdbtnGroup.add(rdbtnCliente);
		rdbtnGroup.add(rdbtnPropietario);
		
		txtAccount = new JTextField();
		txtAccount.setBounds(160, 208, 190, 20);
		txtAccount.setVisible(false);
		txtAccount.setToolTipText("Se espera un valor numérico de 20 dígitos");
		txtAccount.addActionListener(this);
		contentPanel.add(txtAccount);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setBounds(80, 240, 100, 26);
		btnEnviar.addActionListener(this);
		contentPanel.add(btnEnviar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(220, 240, 100, 26);
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		contentPanel.add(btnCancelar);
	}
	
	/**
	 * Default action listener: create a new user
	 */
	public void actionPerformed(ActionEvent e) {
		try {
			if (!Pattern.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", txtEmail.getText())) {
				JOptionPane.showMessageDialog(this, "Introduzca un valor válido para el campo 'Email'", "Advertencia", JOptionPane.WARNING_MESSAGE);
			} else if (MainWindow.getBusinessLogic().emailExists(txtEmail.getText())) {
				JOptionPane.showMessageDialog(this, "Ya existe un usuario registrado con el email introducido", "Advertencia", JOptionPane.WARNING_MESSAGE);
			} else if (pwdPass.getPassword().length == 0) {
				JOptionPane.showMessageDialog(this, "Introduzca un valor para el campo 'Contraseña'", "Advertencia", JOptionPane.WARNING_MESSAGE);
			} else if (!Arrays.equals(pwdPass.getPassword(), pwdPass2.getPassword())) {
				JOptionPane.showMessageDialog(this, "Las contraseñas introducidas no coinciden", "Advertencia", JOptionPane.WARNING_MESSAGE);
			} else if (!Pattern.matches("^\\d{9}$", txtPhone.getText())) {
				JOptionPane.showMessageDialog(this, "Introduzca un valor válido para el campo 'Teléfono': 9 dígitos", "Advertencia", JOptionPane.WARNING_MESSAGE);
			} else if (rdbtnCliente.isSelected()) {
				// Proceder al registro del cliente
				MainWindow.getBusinessLogic().registerNewClient(txtEmail.getText(), pwdPass.getPassword(), txtPhone.getText());
				JOptionPane.showMessageDialog(this, "Cliente registrado correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
				dispose();
			} else if (!Pattern.matches("^\\d{20}$", txtAccount.getText())) {
				JOptionPane.showMessageDialog(this, "Introduzca un valor válido para el campo 'Cuenta bancaria': 20 dígitos", "Advertencia", JOptionPane.WARNING_MESSAGE);
			} else {
				// Proceder al registro del propietario
				MainWindow.getBusinessLogic().registerNewOwner(txtEmail.getText(), pwdPass.getPassword(), txtPhone.getText(), txtAccount.getText());
				JOptionPane.showMessageDialog(this, "Propietario registrado correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
				dispose();
			}
		} catch (RemoteException e1) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
}
