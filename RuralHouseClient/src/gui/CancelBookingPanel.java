package gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import domain.Book;
import domain.Client;
import domain.Offer;
import exceptions.BookNotFound;

public class CancelBookingPanel extends ApplicationPage {

	private static final long serialVersionUID = 1L;

	// Cliente
	private JLabel lblSeleccione;
	private JList<Offer> listBooks;
	private DefaultListModel<Offer> listModel;
	private JScrollPane scrollBooks;
	// Anónimo
	private JLabel lblCodigo;
	private JTextField txtCodigo;
	private JButton btnComprobar;
	private JLabel lblDatosOferta;
	private JLabel lblDatosCasa;
	private JPanel panelImagenes;
	private JScrollPane scrollImagenes;
	// Comunes
	private JButton btnAceptar;
	
	private Vector<Book> reservas;
	private Book reservaElegida;

	public CancelBookingPanel() {
		super("Cancelar una reserva");
		
		// Cliente: Mostrar reservas del cliente que no hayan pasado
		lblSeleccione = new JLabel("Seleccione la reserva que desea cancelar:");
		lblSeleccione.setBounds(50, 80, 400, 14);
		add(lblSeleccione);
		
		listModel = new DefaultListModel<Offer>();
		listBooks = new JList<Offer>(listModel);
		listBooks.setCellRenderer(new OfferCellRenderer());
		listBooks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listBooks.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (!listBooks.isSelectionEmpty()) {
					// Actualizar objeto reserva y activar el botón
					reservaElegida = listBooks.getSelectedValue().getBook();
					btnAceptar.setVisible(true);
				}
			}
		});
		scrollBooks = new JScrollPane(listBooks);
		scrollBooks.setBounds(50, 100, 700, 400);
		add(scrollBooks);
		
		// Anónimo: Pedir un código de reserva
		lblCodigo = new JLabel("Introduzca el código de la reserva que ha realizado:");
		lblCodigo.setHorizontalAlignment(SwingConstants.CENTER);
		lblCodigo.setBounds(200, 80, 400, 14);
		add(lblCodigo);
		
		txtCodigo = new JTextField();
		txtCodigo.setBounds(150, 102, 400, 20);
		add(txtCodigo);
			
		btnComprobar = new JButton("Comprobar");
		btnComprobar.setBounds(575, 100, 125, 24);
		btnComprobar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					reservaElegida = MainWindow.getBusinessLogic().getBook(txtCodigo.getText());
					if (reservaElegida.getOffer().getFirstDay().compareTo(new Date()) <= 0) {
						// Demasiado tarde para cancelar
						JOptionPane.showMessageDialog(MainWindow.getFrame(), "No puede cancelar una reserva cuya fecha de comienzo ya ha pasado", "Advertencia", JOptionPane.WARNING_MESSAGE);
					} else {
						cargarDatosOferta();
					}
				} catch (RemoteException e1) {
					JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				} catch (BookNotFound e2) {
					JOptionPane.showMessageDialog(MainWindow.getFrame(), e2.getMessage(), "Advertencia", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		add(btnComprobar);
		
		lblDatosOferta = new JLabel();
		lblDatosOferta.setHorizontalAlignment(SwingConstants.CENTER);
		lblDatosOferta.setFont(new Font("Arial", Font.BOLD, 13));
		lblDatosOferta.setBounds(75, 150, 650, 20);
		add(lblDatosOferta);
		
		lblDatosCasa = new JLabel();
		lblDatosCasa.setBounds(75, 175, 650, 100);
		lblDatosCasa.setFont(new Font("Verdana", Font.PLAIN, 12));
		add(lblDatosCasa);
		
		panelImagenes = new JPanel();
		scrollImagenes = new JScrollPane(panelImagenes);
		scrollImagenes.setBounds(75, 280, 650, 130);
		add(scrollImagenes);
		
		// Elementos comunes
		try {
			// Cargar la imagen del botón
			BufferedImage iconoAceptar = ImageIO.read(new File("res/button-ok.png"));
			btnAceptar = new JButton(new ImageIcon(iconoAceptar));
			btnAceptar.setBorder(BorderFactory.createEmptyBorder());
			btnAceptar.setContentAreaFilled(false);
			btnAceptar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnAceptar = new JButton("Aceptar");
		}
		btnAceptar.setBounds(300, 530, 200, 50);
		btnAceptar.addActionListener(this);
		add(btnAceptar);

		JButton btnVolver;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-back.png"));
			btnVolver = new JButton(new ImageIcon(iconoVolver));
			btnVolver.setBorder(BorderFactory.createEmptyBorder());
			btnVolver.setContentAreaFilled(false);
			btnVolver.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnVolver = new JButton("Volver");
			btnVolver.setFont(new Font("Tahoma", Font.ITALIC, 14));
			btnVolver.setForeground(Color.BLUE);
		}
		btnVolver.setBounds(25, 5, 110, 43);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(0);
			}
		});
		add(btnVolver);
	}

	@Override
	public boolean init() {
		txtCodigo.setText(null);
		lblDatosOferta.setVisible(false);
		lblDatosCasa.setVisible(false);
		scrollImagenes.setVisible(false);
		btnAceptar.setVisible(false);
		if (MainWindow.getUser() instanceof Client) {
			// Obtiene las reservas del cliente
			try {
				reservas = MainWindow.getBusinessLogic().getFutureBooks((Client) MainWindow.getUser());
				if (reservas.isEmpty()) {
					// Si no hay reservas cancelables, mensaje de error
					JOptionPane.showMessageDialog(MainWindow.getFrame(), "No dispone de ninguna reserva cancelable", "Advertencia", JOptionPane.WARNING_MESSAGE);
					return false;
				} else {
					listModel.clear();
					for (Book b : reservas)
						listModel.addElement(b.getOffer());
					// Mostrar elementos correspondientes
					lblSeleccione.setVisible(true);
					scrollBooks.setVisible(true);
					lblCodigo.setVisible(false);
					txtCodigo.setVisible(false);
					btnComprobar.setVisible(false);
					return true;
				}
			} catch (RemoteException e1) {
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				return false;
			}
		} else {
			// Mostrar elementos correspondientes
			lblSeleccione.setVisible(false);
			scrollBooks.setVisible(false);
			lblCodigo.setVisible(true);
			txtCodigo.setVisible(true);
			btnComprobar.setVisible(true);
			return true;
		}
	}

	/**
	 * Actualiza la interfaz con los datos de la oferta seleccionada.
	 */
	private void cargarDatosOferta() {
		lblDatosOferta.setText(reservaElegida.getOffer().toString());
		lblDatosOferta.setVisible(true);
		lblDatosCasa.setText("<html>Casa rural en " + reservaElegida.getOffer().getRuralHouse().getCity() + "<br>" +
				reservaElegida.getOffer().getRuralHouse().getDescription() + "<br>" +
				reservaElegida.getOffer().getRuralHouse().getRooms() + " habitaciones, " + reservaElegida.getOffer().getRuralHouse().getKitchens() + " cocina(s), " +
				reservaElegida.getOffer().getRuralHouse().getBaths() + " baños, " + reservaElegida.getOffer().getRuralHouse().getLivings() + " sala(s) de estar, " +
				reservaElegida.getOffer().getRuralHouse().getParkings() + " aparcamiento(s)" + "<br>" +
				"Valoración media: " + reservaElegida.getOffer().getRuralHouse().getAverageRating() + "<br>" +
				"Contactar con el propietario: " + reservaElegida.getOffer().getRuralHouse().getOwner().getPhone() + " - " +
				reservaElegida.getOffer().getRuralHouse().getOwner().getEmail() + "</html>");
		lblDatosCasa.setVisible(true);
		panelImagenes.removeAll();
		panelImagenes.revalidate();
		panelImagenes.repaint();
		Vector<byte[]> imagenes = reservaElegida.getOffer().getRuralHouse().getPictures();
		if (imagenes.size() == 0)
			panelImagenes.add(new JLabel("No hay imágenes disponibles de esta casa."));
		else
			for (int i = 0; i < imagenes.size(); i++)
				panelImagenes.add(new JLabel(new ImageIcon(imagenes.get(i))));
		scrollImagenes.setVisible(true);
		btnAceptar.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		try {
			Date sieteDiasAtras = new Date(reservaElegida.getOffer().getFirstDay().getTime() - 604800000);
			String mensaje;
			if (sieteDiasAtras.compareTo(new Date()) > 0) {
				mensaje = "<html>Está a punto de cancelar una reserva para la que falta más de una semana.<br>" +
						"En caso de haber ingresado el depósito, se le devolverá el dinero.<br>" +
						"¿Está seguro de que quiere continuar?</html>";
			} else {
				mensaje = "<html>Está a punto de cancelar una reserva para la que falta menos de una semana.<br>" +
						"No se le va a devolver el dinero que haya depositado hasta el momento.<br>" +
						"¿Está seguro de que quiere continuar?</html>";
			}
			int respuesta = JOptionPane.showOptionDialog(MainWindow.getFrame(), mensaje, "Advertencia", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
			if (respuesta == JOptionPane.YES_OPTION) {
				MainWindow.getBusinessLogic().deleteBook(reservaElegida);
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "La reserva ha sido cancelada satisfactoriamente", "Correcto", JOptionPane.INFORMATION_MESSAGE);
				MainWindow.getFrame().setCurrentPage(0);
			}
		} catch (RemoteException e1) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error de conexión con el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
}