package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;

import domain.RuralHouse;

/**
 * Formato de presentación de las casas rurales en un JList
 */
public class RuralHouseCellRenderer extends JPanel implements ListCellRenderer<RuralHouse> {

	private static final long serialVersionUID = 1L;
	
	private JLabel img;
	private JLabel lblTitulo;
	private JTextArea textDatos;
	private ImageIcon noPictureAvailable;
	
	public RuralHouseCellRenderer() {
		super(null);
		setPreferredSize(new Dimension(680, 110));
		
		try {
			noPictureAvailable = new ImageIcon(ImageIO.read(new File("res/no-photo-available.png")));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			noPictureAvailable = null;
		}
		
		img = new JLabel();
		img.setBounds(10, 5, 134, 100);
		add(img);
		
		lblTitulo = new JLabel();
		lblTitulo.setFont(new Font("Arial", Font.BOLD, 13));
		lblTitulo.setBounds(160, 5, 510, 20);
		add(lblTitulo);

		textDatos = new JTextArea();
		textDatos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		textDatos.setBounds(185, 25, 485, 80);
		textDatos.setEditable(false);
		textDatos.setLineWrap(true);
		textDatos.setWrapStyleWord(true);
		add(textDatos);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 108, 665, 2);
		add(separator);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends RuralHouse> list, RuralHouse value, int index,
			boolean isSelected, boolean cellHasFocus) {
		// Imagen de la casa
		if (value.getPictures().size() == 0) {
			if (noPictureAvailable == null) {
				img.setText("Imagen no disponible");
				img.setIcon(null);
			} else {
				img.setText(null);
				img.setIcon(noPictureAvailable);
			}
		} else {
			img.setText(null);
			img.setIcon(new ImageIcon(
					new ImageIcon(value.getPictures().get(0)).getImage().getScaledInstance(134, 100, Image.SCALE_FAST)));
		}
		// Detalles de la casa
		lblTitulo.setText("Casa número " + value.getHouseNumber() + " en " + value.getCity());
		textDatos.setText(value.getDescription() + "\n" +
				value.getRooms() + " habitaciones, " + value.getKitchens() + " cocina(s), " + value.getBaths() + " baños, " +
				value.getLivings() + " sala(s) de estar, " + value.getParkings() + " aparcamiento(s)" + "\n" +
				value.getOffers().size() + " oferta(s) publicada(s)" + "\n" +
				"Valoración media: " + value.getAverageRating());
		// Resaltar casa seleccionada
		if (isSelected) {
			setBackground(Color.LIGHT_GRAY);
			lblTitulo.setForeground(Color.RED);
			textDatos.setBackground(Color.LIGHT_GRAY);
		} else {
			setBackground(Color.WHITE);
			lblTitulo.setForeground(Color.BLACK);
			textDatos.setBackground(Color.WHITE);
		}
		return this;
	}
}
