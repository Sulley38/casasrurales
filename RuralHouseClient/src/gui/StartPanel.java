package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import domain.Owner;

public class StartPanel extends ApplicationPage {

	private static final long serialVersionUID = 1L;
	
	private JLabel lblImagen;
	private JTabbedPane tabbedPane;
	private JPanel pestanaClient;
	private JPanel pestanaOwner;
	
	/**
	 * Create the panel.
	 */
	public StartPanel() {
		super(null);
		
		// Imagen de la casa
		try {
			BufferedImage imagenCasa = ImageIO.read(new File("res/dibujo-casa.png"));
			lblImagen = new JLabel(new ImageIcon(imagenCasa));
			lblImagen.setBounds(225, 35, 350, 350);
			add(lblImagen);
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
		}
		
		// Panel de pestañas
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(100, 400, 600, 150);
		add(tabbedPane);
		
		pestanaClient = new JPanel(null);
		pestanaClient.setBackground(Color.WHITE);
		tabbedPane.addTab("Operaciones de cliente", null, pestanaClient, null);

		JButton btnBuscarOfertas = new JButton("Buscar ofertas");
		btnBuscarOfertas.setBounds(20, 20, 140, 30);
		btnBuscarOfertas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				((SearchOffersPanel) MainWindow.getFrame().getPage(7)).clear();
				MainWindow.getFrame().setCurrentPage(7);
			}
		});
		pestanaClient.add(btnBuscarOfertas);
		
		JButton btnReservarCasaRural = new JButton("Hacer una reserva");
		btnReservarCasaRural.setBounds(185, 20, 140, 30);
		btnReservarCasaRural.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainWindow.getFrame().setCurrentPage(8);
			}
		});
		pestanaClient.add(btnReservarCasaRural);
		
		JButton btnCancelarReserva = new JButton("Cancelar una reserva");
		btnCancelarReserva.setBounds(20, 65, 140, 30);
		btnCancelarReserva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainWindow.getFrame().setCurrentPage(9);	
			}
		});
		pestanaClient.add(btnCancelarReserva);
		
		JButton btnHacerComentario = new JButton("Hacer un comentario");
		btnHacerComentario.setBounds(185, 65, 140, 30);
		btnHacerComentario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(10);
			}
		});
		pestanaClient.add(btnHacerComentario);
		
		pestanaOwner = new JPanel(null);
		pestanaOwner.setBackground(Color.WHITE);

		JButton btnAnadirCasaRural = new JButton("Añadir una casa");
		btnAnadirCasaRural.setBounds(20, 20, 140, 30);
		btnAnadirCasaRural.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainWindow.getFrame().setCurrentPage(1);
			}
		});
		pestanaOwner.add(btnAnadirCasaRural);
		
		JButton btnEliminarCasaRural = new JButton("Eliminar una casa");
		btnEliminarCasaRural.setBounds(185, 20, 140, 30);
		btnEliminarCasaRural.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(2);
			}
		});
		pestanaOwner.add(btnEliminarCasaRural);
		
		JButton btnGestionarCasas = new JButton("Gestionar casas");
		btnGestionarCasas.setBounds(350, 20, 140, 30);
		btnGestionarCasas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(3);
			}
		});
		pestanaOwner.add(btnGestionarCasas);
		
		JButton btnCrearOferta = new JButton("Crear una oferta");
		btnCrearOferta.setBounds(20, 65, 140, 30);
		btnCrearOferta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(4);
			}
		});
		pestanaOwner.add(btnCrearOferta);
		
		JButton btnEliminarOferta = new JButton("Eliminar una oferta");
		btnEliminarOferta.setBounds(185, 65, 140, 30);
		btnEliminarOferta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(5);
			}
		});
		pestanaOwner.add(btnEliminarOferta);
		
		JButton btnGestionarReservas = new JButton("Gestionar reservas");
		btnGestionarReservas.setBounds(350, 65, 140, 30);
		btnGestionarReservas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(6);
			}
		});
		pestanaOwner.add(btnGestionarReservas);
	}
	
	@Override
	public boolean init() {
		return true;
	}
	
	protected void actualizarPestanas() {
		// Actualiza las pestañas de operaciones
		if (MainWindow.getUser() instanceof Owner) {
			tabbedPane.addTab("Operaciones de propietario", null, pestanaOwner, null);
		} else {
			tabbedPane.remove(pestanaOwner);
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {}

}
