package gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import configuration.Config;
import domain.Comment;

/**
 * Formato de presentación de los comentarios en un JList
 */
public class CommentCellRenderer extends JPanel implements ListCellRenderer<Comment> {

	private static final long serialVersionUID = 1L;
	private JLabel fecha;
	private JTextArea cuerpo;
	private JLabel puntuacion;

	public CommentCellRenderer() {
		super(null);
		setPreferredSize(new Dimension(300, 150));

		fecha = new JLabel();
		fecha.setFont(new Font("Arial", Font.BOLD, 12));
		fecha.setBounds(5, 5, 290, 18);
		add(fecha);
		
		cuerpo = new JTextArea();
		cuerpo.setBackground(UIManager.getColor("Panel.background"));
		cuerpo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cuerpo.setBounds(5, 20, 290, 100);
		cuerpo.setEditable(false);
		cuerpo.setLineWrap(true);
		cuerpo.setWrapStyleWord(true);
		add(cuerpo);
		
		puntuacion = new JLabel();
		puntuacion.setHorizontalAlignment(SwingConstants.CENTER);
		puntuacion.setFont(new Font("Arial", Font.BOLD, 12));
		puntuacion.setBounds(5, 125, 290, 18);
		add(puntuacion);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(5, 147, 290, 2);
		add(separator);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends Comment> list, Comment value, int index,
			boolean isSelected, boolean cellHasFocus) {
		fecha.setText("Comentado el " + Config.getShortDateFormat().format(value.getDate()) + ":");
		cuerpo.setText(value.getText());
		puntuacion.setText(String.format("Puntuación: %.1f sobre 5", value.getRating()));
		return this;
	}
	
}
