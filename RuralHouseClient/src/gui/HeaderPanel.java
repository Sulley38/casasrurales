package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import configuration.Config;
import domain.User;
import exceptions.UserNotFound;

public class HeaderPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JButton btnRegistrarse;
	private JButton btnIniciarSesion;
	private JButton btnModificarPerfil;
	private JButton btnCerrarSesion;
	private JLabel lblBienvenida;
	private JLabel lblEmail;
	private JLabel lblPass;
	private JTextField txtEmail;
	private JPasswordField txtPass;
	
	/**
	 * Create the panel.
	 */
	public HeaderPanel() {
		super(null);
		setBounds(0, 0, 800, 100);
		
		/* Botones */
		btnRegistrarse = new JButton("Registrarse");
		btnRegistrarse.setBounds(645, 65, 130, 25);
		btnRegistrarse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JDialog register = new RegisterDialog();
				register.setVisible(true);
			}
		});
		add(btnRegistrarse);
		
		btnIniciarSesion = new JButton("Iniciar sesión");
		btnIniciarSesion.setBounds(500, 65, 130, 25);
		btnIniciarSesion.addActionListener(this);
		add(btnIniciarSesion);
		
		btnModificarPerfil = new JButton("Modificar perfil");
		btnModificarPerfil.setBounds(645, 65, 130, 25);
		btnModificarPerfil.setVisible(false);
		btnModificarPerfil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JDialog updateProfile = new UpdateProfileDialog();
				updateProfile.setVisible(true);
			}
		});
		add(btnModificarPerfil);
		
		btnCerrarSesion = new JButton("Cerrar sesión");
		btnCerrarSesion.setBounds(500, 65, 130, 25);
		btnCerrarSesion.setVisible(false);
		btnCerrarSesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainWindow.setUser(null);
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Gracias por usar nuestra aplicación", "¡Hasta pronto!", JOptionPane.INFORMATION_MESSAGE);
				actualizarInterfaz(false);
				MainWindow.getFrame().setCurrentPage(0);
			}
		});
		add(btnCerrarSesion);
		
		/* Etiquetas */
		JLabel lblTitulo = new JLabel("<html>Software de gestión de casas rurales</html>");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setFont(new Font("Verdana", Font.BOLD, 18));
		lblTitulo.setBounds(25, 10, 450, 50);
		add(lblTitulo);
		
		JLabel lblFecha = new JLabel("Hoy es " + Config.getFullDateFormat().format(new Date()));
		lblFecha.setForeground(new Color(47, 79, 79));
		lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblFecha.setHorizontalAlignment(SwingConstants.CENTER);
		lblFecha.setBounds(25, 65, 450, 20);
		add(lblFecha);
		
		lblBienvenida = new JLabel();
		lblBienvenida.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblBienvenida.setHorizontalAlignment(SwingConstants.CENTER);
		lblBienvenida.setBounds(500, 12, 270, 45);
		lblBienvenida.setVisible(false);
		add(lblBienvenida);
		
		lblEmail = new JLabel("E-mail:");
		lblEmail.setBounds(505, 10, 60, 20);
		add(lblEmail);
		
		lblPass = new JLabel("Contraseña:");
		lblPass.setBounds(505, 38, 60, 20);
		add(lblPass);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(580, 10, 190, 20);
		txtEmail.addActionListener(this);
		add(txtEmail);
		
		txtPass = new JPasswordField();
		txtPass.setBounds(580, 38, 190, 20);
		txtPass.addActionListener(this);
		add(txtPass);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(25, 98, 750, 2);
		add(separator);
	}
	
	/**
	 * Login action listener.
	 */
	public void actionPerformed(ActionEvent e) {
		// Iniciar sesión
		try {
			User u = MainWindow.getBusinessLogic().login(txtEmail.getText(), txtPass.getPassword());
			MainWindow.setUser(u);
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Ha iniciado sesión correctamente", "Acceso correcto", JOptionPane.INFORMATION_MESSAGE);
			lblBienvenida.setText("<html>Bienvenido, " + u.toString() + "</html>");
			actualizarInterfaz(true);
		} catch (RemoteException e1) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (UserNotFound e2) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error al iniciar sesión: " + e2.getMessage(), "Acceso incorrecto", JOptionPane.WARNING_MESSAGE);
		}
		// Reiniciar campos
		txtEmail.setText(null);
		txtPass.setText(null);
	}

	/**
	 * Mostrar/ocultar los botones y textos de la interfaz al iniciar/cerrar sesión
	 */
	private void actualizarInterfaz(boolean loggedIn) {
		((StartPanel) MainWindow.getFrame().getPage(0)).actualizarPestanas();
		// No logueado
		lblEmail.setVisible(!loggedIn);
		lblPass.setVisible(!loggedIn);
		txtEmail.setVisible(!loggedIn);
		txtPass.setVisible(!loggedIn);
		btnRegistrarse.setVisible(!loggedIn);
		btnIniciarSesion.setVisible(!loggedIn);
		// Logueado
		lblBienvenida.setVisible(loggedIn);
		btnModificarPerfil.setVisible(loggedIn);
		btnCerrarSesion.setVisible(loggedIn);
	}
	
}
