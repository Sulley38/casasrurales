package gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;

import configuration.Config;
import domain.Book;
import domain.Client;
import domain.Offer;
import exceptions.BookNotFound;

public class MakeCommentPanel extends ApplicationPage {

	private static final long serialVersionUID = 1L;
	
	// Cliente
	private JLabel lblEstancia;
	private JList<Offer> listBooks;
	private DefaultListModel<Offer> listModel;
	private JScrollPane scrollBooks;
	// Anónimo
	private JLabel lblCodigo;
	private JTextField txtCodigo;
	private JButton btnComprobar;
	private OfferCellRenderer datosOferta;
	// Comunes
	private JTextArea txtComentario;
	private JScrollPane scrllComentario;
	private JSlider slider;
	private JLabel lblRating;
	private Color verde, rojo;
	private JButton btnAceptar;
	
	private Book reserva = null;

	public MakeCommentPanel() {
		super("Hacer un comentario y valoración");
		
		// Cliente: Mostrar reservas del cliente que hayan pasado
		lblEstancia = new JLabel("Seleccione la que desee comentar de entre sus estancias pasadas sin comentar:");
		lblEstancia.setBounds(50, 75, 400, 15);
		add(lblEstancia);
		
		listModel = new DefaultListModel<Offer>();
		listBooks = new JList<Offer>(listModel);
		listBooks.setCellRenderer(new OfferCellRenderer());
		listBooks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listBooks.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (!listBooks.isSelectionEmpty()) {
					// Actualizar objeto reserva y permitir comentar
					reserva = listBooks.getSelectedValue().getBook();
					txtComentario.setEditable(true);
					slider.setEnabled(true);
					btnAceptar.setVisible(true);
				}
			}
		});
		scrollBooks = new JScrollPane(listBooks);
		scrollBooks.setBounds(50, 100, 700, 250);
		add(scrollBooks);

		// Anónimo: Pedir un código de reserva
		lblCodigo = new JLabel("Introduzca el código de la reserva que ha disfrutado:");
		lblCodigo.setHorizontalAlignment(SwingConstants.CENTER);
		lblCodigo.setBounds(200, 80, 400, 14);
		add(lblCodigo);
		
		txtCodigo = new JTextField();
		txtCodigo.setBounds(150, 102, 400, 20);
		add(txtCodigo);
			
		btnComprobar = new JButton("Comprobar");
		btnComprobar.setBounds(575, 100, 125, 24);
		btnComprobar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					reserva = MainWindow.getBusinessLogic().getBook(txtCodigo.getText());
					if (reserva.getOffer().getLastDay().compareTo(new Date()) >= 0) {
						// La reserva aún no ha pasado
						JOptionPane.showMessageDialog(MainWindow.getFrame(), "Sólo puede comentar una vez terminada la estancia, a partir del día " +
								Config.getShortDateFormat().format(reserva.getOffer().getLastDay()), "Advertencia", JOptionPane.WARNING_MESSAGE);
					} else if (reserva.getCommentMade()) {
						// Ya se ha hecho un comentario de la reserva
						JOptionPane.showMessageDialog(MainWindow.getFrame(), "Sólo se permite un comentario por estancia", "Advertencia", JOptionPane.WARNING_MESSAGE);
					} else {
						// La reserva es correcta, mostrar información y permitir comentar
						datosOferta.getListCellRendererComponent(null, reserva.getOffer(), 0, false, false);
						datosOferta.setVisible(true);
						txtComentario.setEditable(true);
						slider.setEnabled(true);
						btnAceptar.setVisible(true);
					}
				} catch (RemoteException e1) {
					JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				} catch (BookNotFound e2) {
					JOptionPane.showMessageDialog(MainWindow.getFrame(), e2.getMessage(), "Advertencia", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		add(btnComprobar);
			
		datosOferta = new OfferCellRenderer();
		datosOferta.setBounds(60, 175, 680, 110);
		add(datosOferta);

		// Elementos comunes
		JLabel lblComentario = new JLabel("Comentario:");
		lblComentario.setBounds(50, 365, 80, 15);
		add(lblComentario);
		
		txtComentario = new JTextArea(new DefaultStyledDocument() {
			private static final long serialVersionUID = 1L;
			@Override
		    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
		        if ((getLength() + str.length()) <= 280) {
		            super.insertString(offs, str, a);
		        } else {
		            Toolkit.getDefaultToolkit().beep();
		        }
		    }
		});
		txtComentario.setToolTipText("Cuerpo del comentario. Máximo 280 caracteres.");
		scrllComentario = new JScrollPane(txtComentario);
		scrllComentario.setBounds(150, 360, 550, 100);
		add(scrllComentario);

		JLabel lblPuntuacion = new JLabel("Puntuacion global (de 0 a 5):");
		lblPuntuacion.setBounds(50, 470, 150, 25);
		add(lblPuntuacion);
		
		Hashtable<Integer, JLabel> etiquetas = new Hashtable<Integer, JLabel>();
		etiquetas.put(0, new JLabel("0.0"));
		etiquetas.put(10, new JLabel("1.0"));
		etiquetas.put(20, new JLabel("2.0"));
		etiquetas.put(30, new JLabel("3.0"));
		etiquetas.put(40, new JLabel("4.0"));
		etiquetas.put(50, new JLabel("5.0"));
		slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				lblRating.setText(String.format("%.1f", slider.getValue() / 10.0f));
				if (slider.getValue() >= 25)
					lblRating.setForeground(verde);
				else
					lblRating.setForeground(rojo);
			}
		});
		slider.setBounds(200, 470, 500, 50);
		slider.setLabelTable(etiquetas);
		slider.setMinorTickSpacing(1);
		slider.setMajorTickSpacing(10);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setSnapToTicks(true);
		add(slider);
		
		verde = new Color(0, 128, 0);
		rojo = new Color(128, 0, 0);
		lblRating = new JLabel("2.5");
		lblRating.setForeground(verde);
		lblRating.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblRating.setBounds(715, 470, 30, 25);
		add(lblRating);
		
		try {
			// Cargar la imagen del botón
			BufferedImage iconoAceptar = ImageIO.read(new File("res/button-ok.png"));
			btnAceptar = new JButton(new ImageIcon(iconoAceptar));
			btnAceptar.setBorder(BorderFactory.createEmptyBorder());
			btnAceptar.setContentAreaFilled(false);
			btnAceptar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnAceptar = new JButton("Aceptar");
		}
		btnAceptar.setBounds(300, 530, 200, 50);
		btnAceptar.addActionListener(this);
		add(btnAceptar);
		
		JButton btnVolver;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-back.png"));
			btnVolver = new JButton(new ImageIcon(iconoVolver));
			btnVolver.setBorder(BorderFactory.createEmptyBorder());
			btnVolver.setContentAreaFilled(false);
			btnVolver.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnVolver = new JButton("Volver");
			btnVolver.setFont(new Font("Tahoma", Font.ITALIC, 14));
			btnVolver.setForeground(Color.BLUE);
		}
		btnVolver.setBounds(25, 5, 110, 43);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(0);
			}
		});
		add(btnVolver);
	}

	@Override
	public boolean init() {
		// Reiniciar campos
		listModel.clear();
		txtCodigo.setText(null);
		datosOferta.setVisible(false);
		txtComentario.setText(null);
		txtComentario.setEditable(false);
		lblRating.setForeground(verde);
		lblRating.setText("2.5");
		slider.setValue(25);
		slider.setEnabled(false);
		btnAceptar.setVisible(false);
		if (MainWindow.getUser() instanceof Client) {
			// Mostrar elementos correspondientes
			lblEstancia.setVisible(true);
			scrollBooks.setVisible(true);
			lblCodigo.setVisible(false);
			txtCodigo.setVisible(false);
			btnComprobar.setVisible(false);
			// Cargar lista de reservas
			try {
				Vector<Book> reservas = MainWindow.getBusinessLogic().getPastUncommentedBooks((Client) MainWindow.getUser());
				if (reservas.isEmpty()) {
					// Si no hay reservas por comentar, mensaje de error
					JOptionPane.showMessageDialog(MainWindow.getFrame(), "No hay ninguna reserva por comentar asociada a su cuenta", "Advertencia", JOptionPane.WARNING_MESSAGE);
					return false;
				} else {
					for (Book b : reservas)
						listModel.addElement(b.getOffer());
				}
			} catch (RemoteException e1) {
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				return false;
			}
		} else {
			// Mostrar elementos correspondientes
			lblEstancia.setVisible(false);
			scrollBooks.setVisible(false);
			lblCodigo.setVisible(true);
			txtCodigo.setVisible(true);
			btnComprobar.setVisible(true);
		}
		return true;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (txtComentario.getText().isEmpty()) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Introduzca el texto del comentario", "Advertencia", JOptionPane.WARNING_MESSAGE);
		} else {
			try {
				MainWindow.getBusinessLogic().addComment(reserva, txtComentario.getText(), slider.getValue() / 10.0f);
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Comentario añadido. Gracias por su colaboración.", "Correcto", JOptionPane.INFORMATION_MESSAGE);
				MainWindow.getFrame().setCurrentPage(0);
			} catch (RemoteException e1) {
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
}
