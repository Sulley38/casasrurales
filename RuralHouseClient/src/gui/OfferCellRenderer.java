package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;

import domain.Offer;

/**
 * Formato de presentación de las ofertas en un JList
 */
public class OfferCellRenderer extends JPanel implements ListCellRenderer<Offer> {

	private static final long serialVersionUID = 1L;

	private JLabel img;
	private JLabel lblOferta;
	private JTextArea textDatos;
	private ImageIcon noPictureAvailable;
	
	public OfferCellRenderer() {
		super(null);
		setPreferredSize(new Dimension(680, 110));
		
		try {
			noPictureAvailable = new ImageIcon(ImageIO.read(new File("res/no-photo-available.png")));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			noPictureAvailable = null;
		}
		
		img = new JLabel();
		img.setBounds(10, 5, 134, 100);
		add(img);
		
		lblOferta = new JLabel();
		lblOferta.setFont(new Font("Arial", Font.BOLD, 13));
		lblOferta.setBounds(160, 5, 510, 20);
		add(lblOferta);
		
		textDatos = new JTextArea();
		textDatos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		textDatos.setBounds(185, 25, 485, 80);
		textDatos.setEditable(false);
		textDatos.setLineWrap(true);
		textDatos.setWrapStyleWord(true);
		add(textDatos);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 108, 665, 2);
		add(separator);
	}
	
	@Override
	public Component getListCellRendererComponent(JList<? extends Offer> list, Offer value, int index,
			boolean isSelected, boolean cellHasFocus) {
		// Imagen de la casa
		if (value.getRuralHouse().getPictures().size() == 0) {
			if (noPictureAvailable == null) {
				img.setText("Imagen no disponible");
				img.setIcon(null);
			} else {
				img.setText(null);
				img.setIcon(noPictureAvailable);
			}
		} else {
			img.setText(null);
			img.setIcon(new ImageIcon(
					new ImageIcon(value.getRuralHouse().getPictures().get(0)).getImage().getScaledInstance(134, 100, Image.SCALE_FAST)));
		}
		// Detalles de la oferta
		lblOferta.setText(value.toString());
		// Detalles de la casa
		textDatos.setText(value.getRuralHouse().getDescription() + "\n" +
				value.getRuralHouse().getRooms() + " habitaciones, " + value.getRuralHouse().getKitchens() + " cocina(s), " +
				value.getRuralHouse().getBaths() + " baños, " + value.getRuralHouse().getLivings() + " sala(s) de estar, " +
				value.getRuralHouse().getParkings() + " aparcamiento(s)" + "\n" +
				"Valoración media: " + value.getRuralHouse().getAverageRating());
		// Resaltar oferta seleccionada
		if (isSelected) {
			setBackground(Color.LIGHT_GRAY);
			lblOferta.setForeground(Color.RED);
			textDatos.setBackground(Color.LIGHT_GRAY);
		} else {
			setBackground(Color.WHITE);
			lblOferta.setForeground(Color.BLACK);
			textDatos.setBackground(Color.WHITE);
		}
		return this;
	}
	
}
