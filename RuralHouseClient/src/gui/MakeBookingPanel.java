package gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import domain.Book;
import domain.Client;
import domain.Comment;
import domain.Offer;
import exceptions.OfferCanNotBeBooked;
import exceptions.OfferNotFound;

public class MakeBookingPanel extends ApplicationPage {
	
	private static final long serialVersionUID = 1L;
	
	private JButton btnComprobar;
	private JTextField txtCodigoOferta;
	private JLabel lblDatosOferta;
	private JLabel lblDatosCasa;
	private JPanel panelImagenes;
	private JScrollPane scrollImagenes;
	private JList<Comment> listComentarios;
	private JScrollPane scrollComentarios;
	private JLabel lblNoComentarios;
	private JButton btnReservar;
	
	private Offer ofertaElegida;
	private boolean volverAResultadosBusqueda;
	private boolean mantenerCampos;

	public MakeBookingPanel() {
		super("Hacer una reserva");
		volverAResultadosBusqueda = false;
		mantenerCampos = false;
		
		JLabel lblCodigo = new JLabel("Introduzca el código de la oferta:");
		lblCodigo.setBounds(130, 60, 170, 20);
		add(lblCodigo);
		
		txtCodigoOferta = new JTextField();
		txtCodigoOferta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscarOferta();
			}
		});
		txtCodigoOferta.setBounds(300, 59, 200, 20);
		add(txtCodigoOferta);
		
		btnComprobar = new JButton("Comprobar");
		btnComprobar.setBounds(520, 57, 125, 24);
		btnComprobar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscarOferta();
			}
		});
		add(btnComprobar);
		
		lblDatosOferta = new JLabel();
		lblDatosOferta.setHorizontalAlignment(SwingConstants.CENTER);
		lblDatosOferta.setFont(new Font("Arial", Font.BOLD, 13));
		lblDatosOferta.setBounds(50, 90, 700, 20);
		add(lblDatosOferta);
		
		lblDatosCasa = new JLabel();
		lblDatosCasa.setBounds(50, 112, 700, 100);
		lblDatosCasa.setFont(new Font("Verdana", Font.PLAIN, 12));
		add(lblDatosCasa);
		
		panelImagenes = new JPanel();
		panelImagenes.setLayout(new BoxLayout(panelImagenes, BoxLayout.Y_AXIS));
		scrollImagenes = new JScrollPane(panelImagenes);
		scrollImagenes.setBounds(25, 215, 419, 310);
		add(scrollImagenes);
		
		listComentarios = new JList<Comment>();
		listComentarios.setBackground(UIManager.getColor("Panel.background"));
		listComentarios.setCellRenderer(new CommentCellRenderer());
		scrollComentarios = new JScrollPane(listComentarios);
		scrollComentarios.setBounds(450, 215, 320, 310);
		add(scrollComentarios);
		
		lblNoComentarios = new JLabel("<html>No hay comentarios sobre esta casa.<html>");
		lblNoComentarios.setHorizontalAlignment(SwingConstants.CENTER);
		lblNoComentarios.setBounds(460, 340, 300, 60);
		add(lblNoComentarios);
		
		try {
			// Cargar la imagen del botón
			BufferedImage iconoAceptar = ImageIO.read(new File("res/button-book.png"));
			btnReservar = new JButton(new ImageIcon(iconoAceptar));
			btnReservar.setBorder(BorderFactory.createEmptyBorder());
			btnReservar.setContentAreaFilled(false);
			btnReservar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnReservar = new JButton("Reservar");
		}
		btnReservar.setBounds(300, 535, 200, 50);
		btnReservar.addActionListener(this);
		add(btnReservar);
		
		JButton btnVolver;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-back.png"));
			btnVolver = new JButton(new ImageIcon(iconoVolver));
			btnVolver.setBorder(BorderFactory.createEmptyBorder());
			btnVolver.setContentAreaFilled(false);
			btnVolver.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnVolver = new JButton("Volver");
			btnVolver.setFont(new Font("Tahoma", Font.ITALIC, 14));
			btnVolver.setForeground(Color.BLUE);
		}
		btnVolver.setBounds(25, 5, 110, 43);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (volverAResultadosBusqueda)
					MainWindow.getFrame().setCurrentPage(7);
				else
					MainWindow.getFrame().setCurrentPage(0);
			}
		});
		add(btnVolver);
	}

	@Override
	public boolean init() {
		if (!mantenerCampos) {
			txtCodigoOferta.setText(null);
			txtCodigoOferta.setEditable(true);
			btnComprobar.setEnabled(true);
			lblDatosOferta.setVisible(false);
			lblDatosCasa.setVisible(false);
			scrollImagenes.setVisible(false);
			scrollComentarios.setVisible(false);
			lblNoComentarios.setVisible(false);
			btnReservar.setVisible(false);
			volverAResultadosBusqueda = false;
		} else {
			mantenerCampos = false;
		}
		return true;
	}

	/**
	 * Inicializa la interfaz con los datos de la oferta que se pasa por parámetro. Se llama desde SearchOffersPanel.
	 */
	public boolean init(Offer oferta) {
		ofertaElegida = oferta;
		txtCodigoOferta.setText(Integer.toString(ofertaElegida.getOfferNumber()));
		txtCodigoOferta.setEditable(false);
		btnComprobar.setEnabled(false);
		cargarDatosOferta();
		volverAResultadosBusqueda = true;
		mantenerCampos = true;
		return true;
	}
	
	/**
	 * Busca la oferta correspondiente al código introducido.
	 */
	private void buscarOferta() {
		try {
			int codOffer = Integer.parseInt(txtCodigoOferta.getText());
			ofertaElegida = MainWindow.getBusinessLogic().getOffer(codOffer);
			cargarDatosOferta();
		} catch (RemoteException e1) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (NumberFormatException e2) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Debe introducir un valor entero", "Advertencia", JOptionPane.WARNING_MESSAGE);
		} catch (OfferNotFound e3) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), e3.getMessage(), "Advertencia", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * Actualiza la interfaz con los datos de la oferta seleccionada.
	 */
	private void cargarDatosOferta() {
		lblDatosOferta.setText(ofertaElegida.toString());
		lblDatosOferta.setVisible(true);
		lblDatosCasa.setText("<html>Casa rural en " + ofertaElegida.getRuralHouse().getCity() + "<br>" +
				ofertaElegida.getRuralHouse().getDescription() + "<br>" +
				ofertaElegida.getRuralHouse().getRooms() + " habitaciones, " + ofertaElegida.getRuralHouse().getKitchens() + " cocina(s), " +
				ofertaElegida.getRuralHouse().getBaths() + " baños, " + ofertaElegida.getRuralHouse().getLivings() + " sala(s) de estar, " +
				ofertaElegida.getRuralHouse().getParkings() + " aparcamiento(s)" + "<br>" +
				"Valoración media: " + ofertaElegida.getRuralHouse().getAverageRating() + "<br>" +
				"Contactar con el propietario: " + ofertaElegida.getRuralHouse().getOwner().getPhone() + " - " +
				ofertaElegida.getRuralHouse().getOwner().getEmail() + "</html>");
		lblDatosCasa.setVisible(true);
		panelImagenes.removeAll();
		panelImagenes.revalidate();
		panelImagenes.repaint();
		Vector<byte[]> imagenes = ofertaElegida.getRuralHouse().getPictures();
		if (imagenes.size() == 0)
			panelImagenes.add(new JLabel("No hay imágenes disponibles de esta casa."));
		else
			for (int i = 0; i < imagenes.size(); i++)
				panelImagenes.add(new JLabel(new ImageIcon(imagenes.get(i))));
		scrollImagenes.setVisible(true);
		if (ofertaElegida.getRuralHouse().getComments().size() == 0) {
			scrollComentarios.setVisible(false);
			lblNoComentarios.setVisible(true);
		} else {
			listComentarios.setListData(ofertaElegida.getRuralHouse().getComments());
			scrollComentarios.setVisible(true);
			lblNoComentarios.setVisible(false);
		}
		btnReservar.setVisible(true);
	}
	
	/**
	 * Default action listener: crear la reserva y mostrar el diálogo de confirmación
	 */
	public void actionPerformed(ActionEvent e) {
		try {
			if (MainWindow.getUser() instanceof Client) {
				// Crear la reserva a nombre del cliente
				Book book = MainWindow.getBusinessLogic().createBook(ofertaElegida, (Client) MainWindow.getUser());
				// Abrir el cuadro de diálogo de confirmación
				JDialog dialogo = new MakeBookingConfirmationDialog(book);
				dialogo.setVisible(true);
			} else {
				// Reserva anónima
				int respuesta = JOptionPane.showOptionDialog(MainWindow.getFrame(), 
						"<html>Está a punto de realizar una reserva de forma anónima.<br>" +
						"Si continúa, no recibirá actualizaciones via e-mail sobre el estado de su pedido y necesitará el código de reserva para cualquier operación.<br>" +
						"Por su seguridad y comodidad, le recomendamos que inicie sesión con una cuenta de cliente antes de realizar una reserva.<br>" +
						"¿Está seguro de que quiere continuar?</html>", "Advertencia", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE, null, null, null);
				if (respuesta == JOptionPane.YES_OPTION) {
					Book book = MainWindow.getBusinessLogic().createBook(ofertaElegida, null);
					// Abrir el cuadro de diálogo de confirmación
					JDialog dialogo = new MakeBookingConfirmationDialog(book);
					dialogo.setVisible(true);
				}
			}
		} catch (OfferCanNotBeBooked e1) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "No se puede reservar esa oferta: " + e1.getMessage(), "Advertencia", JOptionPane.WARNING_MESSAGE);
		} catch (RemoteException e2) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e2.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
}
