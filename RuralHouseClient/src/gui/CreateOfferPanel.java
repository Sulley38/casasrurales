package gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JFormattedTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

import org.jdesktop.swingx.JXDatePicker;

import configuration.Config;
import domain.Offer;
import domain.Owner;
import domain.RuralHouse;

public class CreateOfferPanel extends ApplicationPage {
	
	private static final long serialVersionUID = 1L;
	
	private JList<RuralHouse> lstCasas;
	private DefaultListModel<RuralHouse> listModel;
	private JScrollPane scrollPane;
	private JXDatePicker datePickerInicio;
	private JXDatePicker datePickerFin;
	private JFormattedTextField ftxtPrecio;

	public CreateOfferPanel() {
		super("Crear una oferta");
		
		JLabel lblListado = new JLabel("Seleccione para qué casa va a crear la oferta:");
		lblListado.setBounds(50, 65, 300, 14);
		add(lblListado);
		
		listModel = new DefaultListModel<RuralHouse>();
		lstCasas = new JList<RuralHouse>(listModel);
		lstCasas.setCellRenderer(new RuralHouseCellRenderer());
		lstCasas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstCasas.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (!lstCasas.isSelectionEmpty()) {
					// Tachar en el calendario las fechas en las que la casa está reservada
					SortedSet<Date> diasReservada = new TreeSet<Date>();
					Calendar cal = Calendar.getInstance();
					for (Offer oferta : lstCasas.getSelectedValue().getOffers()) {
						if (oferta.getBook() != null) {
							for (cal.setTime(oferta.getFirstDay()); !cal.getTime().equals(oferta.getLastDay()); cal.add(Calendar.DAY_OF_MONTH, 1))
								diasReservada.add(cal.getTime());
							diasReservada.add(cal.getTime());
						}
					}
					datePickerInicio.getMonthView().getSelectionModel().setUnselectableDates(diasReservada);
					datePickerInicio.setDate(null);
					datePickerFin.setDate(null);
				}
			}
		});
		scrollPane = new JScrollPane(lstCasas);
		scrollPane.setBounds(50, 90, 700, 380);
		add(scrollPane);
		
		JLabel lblFechaInicio = new JLabel("Fecha de entrada:");
		lblFechaInicio.setBounds(105, 490, 100, 20);
		add(lblFechaInicio);
		
		datePickerInicio = new JXDatePicker(Config.getLocale());
		datePickerInicio.setBounds(205, 490, 100, 20);
		datePickerInicio.setFormats("d MMM YYYY");
		datePickerInicio.setLinkDay(new Date(), "Hoy es {0,date,d 'de' MMMM 'del' y}");
		datePickerInicio.getEditor().setEditable(false);
		datePickerInicio.getMonthView().setLowerBound(new Date());
		datePickerInicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (datePickerInicio.getDate() != null) {
					// Como mínimo un día después de la fecha de inicio
					Date diaDespues = new Date(datePickerInicio.getDate().getTime() + 86400000);
					datePickerFin.getMonthView().setLowerBound(diaDespues);
					datePickerFin.getMonthView().setFirstDisplayedDay(diaDespues);
					// Como máximo un día antes de la próxima reserva, si la hay
					if (!lstCasas.isSelectionEmpty()) {
						Date inicioReserva = null;
						for (Offer oferta : lstCasas.getSelectedValue().getOffers()) {
							if (oferta.getBook() != null && oferta.getFirstDay().compareTo(datePickerInicio.getDate()) > 0
									&& (inicioReserva == null || oferta.getFirstDay().compareTo(inicioReserva) < 0))
								inicioReserva = oferta.getFirstDay();
						}
						if (inicioReserva != null) // Hay reserva posterior
							datePickerFin.getMonthView().setUpperBound(new Date(inicioReserva.getTime() - 86400000));
						else // No la hay
							datePickerFin.getMonthView().setUpperBound(null);
					}
				} else {
					datePickerFin.setDate(null);
				}
			}
		});
		add(datePickerInicio);
		
		JLabel lblFechaFin = new JLabel("Fecha de salida:");
		lblFechaFin.setBounds(335, 490, 100, 20);
		add(lblFechaFin);
		
		datePickerFin = new JXDatePicker(Config.getLocale());
		datePickerFin.setBounds(425, 490, 100, 20);
		datePickerFin.setFormats("d MMM YYYY");
		datePickerFin.setLinkDay(new Date(), "Hoy es {0,date,d 'de' MMMM 'del' y}");
		datePickerFin.getEditor().setEditable(false);
		add(datePickerFin);
		
		JLabel lblPrecio = new JLabel("Precio (€):");
		lblPrecio.setBounds(555, 490, 60, 20);
		add(lblPrecio);
		
		ftxtPrecio = new JFormattedTextField(Config.getDecimalFormat());
		ftxtPrecio.setBounds(615, 490, 80, 20);
		ftxtPrecio.setToolTipText("Use la coma (,) como separador de decimales");
		ftxtPrecio.setLocale(Config.getLocale());
		ftxtPrecio.addActionListener(this);
		add(ftxtPrecio);

		JButton btnAceptar;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoAceptar = ImageIO.read(new File("res/button-ok.png"));
			btnAceptar = new JButton(new ImageIcon(iconoAceptar));
			btnAceptar.setBorder(BorderFactory.createEmptyBorder());
			btnAceptar.setContentAreaFilled(false);
			btnAceptar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnAceptar = new JButton("Aceptar");
		}
		btnAceptar.setBounds(300, 530, 200, 50);
		btnAceptar.addActionListener(this);
		add(btnAceptar);
		
		JButton btnVolver;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-back.png"));
			btnVolver = new JButton(new ImageIcon(iconoVolver));
			btnVolver.setBorder(BorderFactory.createEmptyBorder());
			btnVolver.setContentAreaFilled(false);
			btnVolver.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnVolver = new JButton("Volver");
			btnVolver.setFont(new Font("Tahoma", Font.ITALIC, 14));
			btnVolver.setForeground(Color.BLUE);
		}
		btnVolver.setBounds(25, 5, 110, 43);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(0);
			}
		});
		add(btnVolver);
	}
	
	@Override
	public boolean init() {
		try {
			// Obtiene las casas rurales del propietario
			Vector<RuralHouse> casas = MainWindow.getBusinessLogic().getRuralHouses((Owner) MainWindow.getUser());
			if (casas.isEmpty()) {
				// Si no hay casas, mensaje de error
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "No dispone de ninguna casa rural en el sistema", "Advertencia", JOptionPane.WARNING_MESSAGE);
				return false;
			} else {
				listModel.clear();
				for (RuralHouse rh : casas)
					listModel.addElement(rh);
				// Reiniciar campos
				datePickerInicio.setDate(null);
				datePickerInicio.getMonthView().getSelectionModel().setUnselectableDates(new TreeSet<Date>());
				datePickerFin.setDate(null);
				datePickerFin.getMonthView().setLowerBound(new Date());
				datePickerFin.getMonthView().setUpperBound(null);
				ftxtPrecio.setValue(new Float("0.0"));
				return true;
			}
		} catch (RemoteException e1) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			// Leer el precio introducido en la caja de texto
			float precioIntroducido = Config.getDecimalFormat().parse(ftxtPrecio.getText()).floatValue();
			if (lstCasas.isSelectionEmpty()) {
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Seleccione una casa", "Advertencia", JOptionPane.WARNING_MESSAGE);
			} else if (datePickerInicio.getDate() == null) {
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Seleccione una fecha de entrada", "Advertencia", JOptionPane.WARNING_MESSAGE);
			} else if (datePickerFin.getDate() == null) {
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Seleccione una fecha de salida", "Advertencia", JOptionPane.WARNING_MESSAGE);
			} else if (precioIntroducido < 0.01) {
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Introduzca un precio válido", "Advertencia", JOptionPane.WARNING_MESSAGE);
			} else {
				MainWindow.getBusinessLogic().createOffer(lstCasas.getSelectedValue(), datePickerInicio.getDate(), datePickerFin.getDate(), precioIntroducido);
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Oferta añadida con éxito", "Correcto", JOptionPane.INFORMATION_MESSAGE);
				MainWindow.getFrame().setCurrentPage(0);
			}
		} catch (ParseException e1) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Introduzca un precio válido", "Advertencia", JOptionPane.WARNING_MESSAGE);
		} catch (RemoteException e2) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e2.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
}