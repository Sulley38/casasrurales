package gui;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.regex.Pattern;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

import domain.Owner;
import domain.User;

public class UpdateProfileDialog extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField txtEmail;
	private JPasswordField pwdPass;
	private JPasswordField pwdPass2;
	private JTextField txtPhone;
	private JTextField txtAccount;
	private User currentUser;
	
	/**
	 * Create the dialog.
	 */
	public UpdateProfileDialog() {
		super(MainWindow.getFrame(), true);
		currentUser = MainWindow.getUser();
		
		setSize(400, 280);
		setResizable(false);
		setLocationRelativeTo(MainWindow.getFrame());
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle("Modificar perfil del usuario");
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(30, 30, 120, 16);
		contentPanel.add(lblEmail);
		
		JLabel lblPass = new JLabel("Contraseña:");
		lblPass.setBounds(30, 60, 120, 16);
		contentPanel.add(lblPass);
		
		JLabel lblPassStar = new JLabel("*");
		lblPassStar.setForeground(Color.RED);
		lblPassStar.setBounds(352, 58, 8, 16);
		contentPanel.add(lblPassStar);
		
		JLabel lblPass2 = new JLabel("Confirmar contraseña:");
		lblPass2.setBounds(30, 90, 120, 16);
		contentPanel.add(lblPass2);
		
		JLabel lblPass2Star = new JLabel("*");
		lblPass2Star.setForeground(Color.RED);
		lblPass2Star.setBounds(352, 88, 8, 16);
		contentPanel.add(lblPass2Star);
		
		JLabel lblNota = new JLabel("* Deje en blanco si no desea modificar la contraseña");
		lblNota.setForeground(Color.RED);
		lblNota.setBounds(65, 118, 260, 16);
		contentPanel.add(lblNota);
		
		JLabel lblPhone = new JLabel("Teléfono:");
		lblPhone.setBounds(30, 150, 120, 16);
		contentPanel.add(lblPhone);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(160, 28, 190, 20);
		txtEmail.setEditable(false);
		txtEmail.setText(currentUser.getEmail());
		contentPanel.add(txtEmail);
		
		pwdPass = new JPasswordField();
		pwdPass.setBounds(160, 58, 190, 20);
		pwdPass.addActionListener(this);
		contentPanel.add(pwdPass);
		
		pwdPass2 = new JPasswordField();
		pwdPass2.setBounds(160, 88, 190, 20);
		pwdPass2.addActionListener(this);
		contentPanel.add(pwdPass2);
		
		txtPhone = new JTextField();
		txtPhone.setBounds(160, 148, 190, 20);
		txtPhone.setText(currentUser.getPhone());
		txtPhone.setToolTipText("Se espera un valor numérico de 9 dígitos");
		txtPhone.addActionListener(this);
		contentPanel.add(txtPhone);
		
		if (currentUser instanceof Owner) { // Únicamente visible para propietarios
			JLabel lblAccount = new JLabel("Cuenta bancaria:");
			lblAccount.setBounds(30, 180, 120, 16);
			contentPanel.add(lblAccount);
			
			txtAccount = new JTextField();
			txtAccount.setBounds(160, 178, 190, 20);
			txtAccount.setText(((Owner) currentUser).getBankAccount());
			txtAccount.setToolTipText("Se espera un valor numérico de 20 dígitos");
			txtAccount.addActionListener(this);
			contentPanel.add(txtAccount);
		}
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(80, 210, 100, 26);
		btnGuardar.addActionListener(this);
		contentPanel.add(btnGuardar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(220, 210, 100, 26);
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		contentPanel.add(btnCancelar);
	}

	/**
	 * Default action listener: save profile changes
	 */
	public void actionPerformed(ActionEvent e) {
		try {
			if ((pwdPass.getPassword().length != 0 || pwdPass2.getPassword().length != 0) && !Arrays.equals(pwdPass.getPassword(), pwdPass2.getPassword())) {
				JOptionPane.showMessageDialog(this, "Las contraseñas introducidas no coinciden", "Advertencia", JOptionPane.WARNING_MESSAGE);
			} else if (!Pattern.matches("^\\d{9}$", txtPhone.getText())) {
				JOptionPane.showMessageDialog(this, "Introduzca un valor válido para el campo 'Teléfono': 9 dígitos", "Advertencia", JOptionPane.WARNING_MESSAGE);
			} else if ((currentUser instanceof Owner) && !Pattern.matches("^\\d{20}$", txtAccount.getText())) {
				JOptionPane.showMessageDialog(this, "Introduzca un valor válido para el campo 'Cuenta bancaria': 20 dígitos", "Advertencia", JOptionPane.WARNING_MESSAGE);
			} else {
				// Actualizar objeto remoto
				MainWindow.getBusinessLogic().updateUser(currentUser, (pwdPass.getPassword().length > 0) ? pwdPass.getPassword() : null, txtPhone.getText(),
						(currentUser instanceof Owner) ? txtAccount.getText() : null);
				// Actualizar objeto local
				if (pwdPass.getPassword().length > 0)
					currentUser.setPassword(pwdPass.getPassword());
				currentUser.setPhone(txtPhone.getText());
				if (currentUser instanceof Owner)
					((Owner) currentUser).setBankAccount(txtAccount.getText());
				JOptionPane.showMessageDialog(this, "Datos del perfil actualizados con éxito", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
				dispose();
			}
		} catch (RemoteException e1) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
}
