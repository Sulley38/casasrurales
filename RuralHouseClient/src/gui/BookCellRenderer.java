package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.ListCellRenderer;

import configuration.Config;
import domain.Book;
import java.awt.SystemColor;

/**
 * Formato de presentación de las reservas en un JList
 */
public class BookCellRenderer extends JPanel implements ListCellRenderer<Book> {

	private static final long serialVersionUID = 1L;
	private JLabel lblReserva;
	private JLabel lblOferta;
	private JLabel lblEstado;
	
	public BookCellRenderer() {
		super(null);
		setPreferredSize(new Dimension(680, 80));
		
		lblReserva = new JLabel();
		lblReserva.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblReserva.setBounds(15, 5, 650, 32);
		add(lblReserva);
		
		lblOferta = new JLabel();
		lblOferta.setBounds(15, 40, 650, 15);
		add(lblOferta);
		
		lblEstado = new JLabel();
		lblEstado.setBounds(15, 58, 650, 15);
		add(lblEstado);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(20, 78, 640, 2);
		add(separator);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends Book> list, Book value, int index,
			boolean isSelected, boolean cellHasFocus) {
		lblReserva.setText("<html>Reserva " + value.getBookCode() + "<br>realizada el " + Config.getShortDateFormat().format(value.getBookDate()) + " por " +
				((value.getClient() != null) ? value.getClient().getEmail() + " (" + value.getClient().getPhone() + ")" : "un cliente anónimo") + "</html>");
		lblOferta.setText("de la oferta " + value.getOffer().getOfferNumber() + " (del " + Config.getShortDateFormat().format(value.getOffer().getFirstDay()) +
				" al " + Config.getShortDateFormat().format(value.getOffer().getLastDay()) + " por " + Config.getDecimalFormat().format(value.getOffer().getPrice()) + "€) " +
				"publicada en la casa número " + value.getOffer().getRuralHouse().getHouseNumber() + " (" + value.getOffer().getRuralHouse().getCity() + ")");
		lblEstado.setText("Estado de la reserva: " + Book.statusText[value.getStatus()]);
		if (isSelected)
			setBackground(SystemColor.activeCaption);
		else
			setBackground(Color.WHITE);
		return this;
	}
}
