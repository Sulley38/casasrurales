package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import domain.Owner;

public class AddRuralHousePanel extends ApplicationPage {

	private static final long serialVersionUID = 1L;
	
	private JTextField txtLocalidad;
	private JTextArea txtDescripcion;
	private JScrollPane scrllDescripcion;
	private JSpinner spnHabitaciones;
	private JSpinner spnCocinas;
	private JSpinner spnBanos;
	private JSpinner spnSalas;
	private JSpinner spnAparcamientos;
	private JList<ImageIcon> listImages;
	private DefaultListModel<ImageIcon> listModel;
	private JScrollPane scrollImages;
	
	/**
	 * Create the panel.
	 */
	public AddRuralHousePanel() {
		super("Añadir una casa");
		
		JLabel lblLocalidad = new JLabel("Localidad:");
		lblLocalidad.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLocalidad.setBounds(120, 65, 100, 22);
		add(lblLocalidad);
		
		txtLocalidad = new JTextField();
		txtLocalidad.setToolTipText("Este campo es obligatorio");
		txtLocalidad.setBounds(230, 65, 400, 22);
		txtLocalidad.addActionListener(this);
		add(txtLocalidad);
		
		JLabel lblDescripcion = new JLabel("Descripción:");
		lblDescripcion.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDescripcion.setBounds(120, 100, 100, 22);
		add(lblDescripcion);
		
		txtDescripcion = new JTextArea(new DefaultStyledDocument() {
			private static final long serialVersionUID = 1L;
			@Override
		    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
		        if ((getLength() + str.length()) <= 160) {
		            super.insertString(offs, str, a);
		        } else {
		            Toolkit.getDefaultToolkit().beep();
		        }
		    }
		});
		txtDescripcion.setToolTipText("Este campo es opcional. Máximo 160 caracteres.");
		scrllDescripcion = new JScrollPane(txtDescripcion);
		scrllDescripcion.setBounds(230, 100, 400, 66);
		add(scrllDescripcion);
		
		JLabel lblHabitaciones = new JLabel("Habitaciones:");
		lblHabitaciones.setHorizontalAlignment(SwingConstants.RIGHT);
		lblHabitaciones.setBounds(120, 180, 100, 22);
		add(lblHabitaciones);
		
		spnHabitaciones = new JSpinner(new SpinnerNumberModel(3, 3, 50, 1));
		spnHabitaciones.setToolTipText("Introduzca un valor igual o superior a 3");
		spnHabitaciones.setBounds(230, 180, 40, 22);
		add(spnHabitaciones);
		
		JLabel lblCocinas = new JLabel("Cocinas:");
		lblCocinas.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCocinas.setBounds(120, 220, 100, 22);
		add(lblCocinas);
		
		spnCocinas = new JSpinner(new SpinnerNumberModel(1, 1, 50, 1));
		spnCocinas.setToolTipText("Introduzca un valor igual o superior a 1");
		spnCocinas.setBounds(230, 220, 40, 22);
		add(spnCocinas);
		
		JLabel lblBanos = new JLabel("Baños:");
		lblBanos.setHorizontalAlignment(SwingConstants.RIGHT);
		lblBanos.setBounds(120, 260, 100, 22);
		add(lblBanos);
		
		spnBanos = new JSpinner(new SpinnerNumberModel(2, 2, 50, 1));
		spnBanos.setToolTipText("Introduzca un valor igual o superior a 2");
		spnBanos.setBounds(230, 260, 40, 22);
		add(spnBanos);
		
		JLabel lblSalas = new JLabel("Salas de estar:");
		lblSalas.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSalas.setBounds(340, 180, 100, 22);
		add(lblSalas);
		
		spnSalas = new JSpinner(new SpinnerNumberModel(0, 0, 50, 1));
		spnSalas.setToolTipText("Introduzca un valor igual o superior a 0");
		spnSalas.setBounds(450, 180, 40, 22);
		add(spnSalas);
		
		JLabel lblAparcamientos = new JLabel("Aparcamientos:");
		lblAparcamientos.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAparcamientos.setBounds(340, 220, 100, 22);
		add(lblAparcamientos);
		
		spnAparcamientos = new JSpinner(new SpinnerNumberModel(0, 0, 50, 1));
		spnAparcamientos.setToolTipText("Introduzca un valor igual o superior a 0");
		spnAparcamientos.setBounds(450, 220, 40, 22);
		add(spnAparcamientos);
		
		JPanel panelImagenes = new JPanel(null);
		panelImagenes.setBorder(BorderFactory.createTitledBorder("Imágenes (opcional):"));
		panelImagenes.setBounds(50, 300, 700, 180);
		add(panelImagenes);
		
		listModel = new DefaultListModel<ImageIcon>();
		listImages = new JList<ImageIcon>(listModel);
		listImages.setCellRenderer(new DefaultListCellRenderer() {
			private static final long serialVersionUID = 1L;
			@Override
			public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				setHorizontalAlignment(SwingConstants.CENTER);
				setVerticalAlignment(SwingConstants.CENTER);
				Image icono = ((ImageIcon) value).getImage().getScaledInstance(-1, 100, Image.SCALE_FAST);
				setIcon(new ImageIcon(icono));
				setPreferredSize(new Dimension(getIcon().getIconWidth() + 10, 110));
				return this;
			}
		});
		listImages.setLayoutOrientation(JList.VERTICAL_WRAP);
		listImages.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listImages.setVisibleRowCount(-1);
		scrollImages = new JScrollPane(listImages);
		scrollImages.setBounds(20, 20, 620, 130);
		panelImagenes.add(scrollImages);
		
		JButton btnAnadir;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-add.png"));
			btnAnadir = new JButton(new ImageIcon(iconoVolver));
			btnAnadir.setBorder(BorderFactory.createEmptyBorder());
			btnAnadir.setContentAreaFilled(false);
			btnAnadir.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnAnadir = new JButton("+");
			btnAnadir.setFont(new Font("Tahoma", Font.ITALIC, 14));
			btnAnadir.setForeground(Color.GREEN);
		}
		btnAnadir.setBounds(650, 20, 32, 32);
		btnAnadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Mostrar diálogo de Abrir archivo
				FileNameExtensionFilter filtro = new FileNameExtensionFilter("Imágenes (*.bmp, *.gif, *.jpg, *.png)", "bmp", "gif", "jpg", "png");
				JFileChooser fc = new JFileChooser();
				fc.setFileFilter(filtro);
				fc.setMultiSelectionEnabled(true);
				int respuesta = fc.showOpenDialog(MainWindow.getFrame());
				if (respuesta == JFileChooser.APPROVE_OPTION) {
					// Añadir las nuevas imágenes
					for (File f : fc.getSelectedFiles()) {
						try {
							BufferedImage imgOriginal = ImageIO.read(f), imgRedimensionada = new BufferedImage(400, Math.round(imgOriginal.getHeight() * 400 / imgOriginal.getWidth()), BufferedImage.TYPE_INT_RGB);
							Graphics2D g = imgRedimensionada.createGraphics();
							g.drawImage(imgOriginal, 0, 0, 400, Math.round(imgOriginal.getHeight() * 400 / imgOriginal.getWidth()), null);
							g.dispose();
							listModel.addElement(new ImageIcon(imgRedimensionada));
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error al cargar la imagen: " + e1.getMessage(), "Error", JOptionPane.WARNING_MESSAGE);
						}
					}
				}
			}
		});
		panelImagenes.add(btnAnadir);
		
		JButton btnQuitar;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-remove.png"));
			btnQuitar = new JButton(new ImageIcon(iconoVolver));
			btnQuitar.setBorder(BorderFactory.createEmptyBorder());
			btnQuitar.setContentAreaFilled(false);
			btnQuitar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnQuitar = new JButton("-");
			btnQuitar.setFont(new Font("Tahoma", Font.ITALIC, 14));
			btnQuitar.setForeground(Color.RED);
		}
		btnQuitar.setBounds(650, 60, 32, 32);
		btnQuitar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Eliminar la imagen seleccionada
				if (!listImages.isSelectionEmpty())
					listModel.removeElementAt(listImages.getSelectedIndex());
			}
		});
		panelImagenes.add(btnQuitar);
		
		JLabel lblNota = new JLabel("Las imágenes son redimensionadas de forma automática. La primera se usará como portada de la casa.");
		lblNota.setHorizontalAlignment(SwingConstants.CENTER);
		lblNota.setBounds(30, 155, 600, 15);
		panelImagenes.add(lblNota);
		
		JButton btnAceptar;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoAceptar = ImageIO.read(new File("res/button-ok.png"));
			btnAceptar = new JButton(new ImageIcon(iconoAceptar));
			btnAceptar.setBorder(BorderFactory.createEmptyBorder());
			btnAceptar.setContentAreaFilled(false);
			btnAceptar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnAceptar = new JButton("Aceptar");
		}
		btnAceptar.setBounds(300, 520, 200, 50);
		btnAceptar.addActionListener(this);
		add(btnAceptar);
		
		JButton btnVolver;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-back.png"));
			btnVolver = new JButton(new ImageIcon(iconoVolver));
			btnVolver.setBorder(BorderFactory.createEmptyBorder());
			btnVolver.setContentAreaFilled(false);
			btnVolver.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnVolver = new JButton("Volver");
			btnVolver.setFont(new Font("Tahoma", Font.ITALIC, 14));
			btnVolver.setForeground(Color.BLUE);
		}
		btnVolver.setBounds(25, 5, 110, 43);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(0);
			}
		});
		add(btnVolver);
	}

	@Override
	public boolean init() {
		txtLocalidad.setText(null);
		txtDescripcion.setText(null);
		spnHabitaciones.setValue(3);
		spnCocinas.setValue(1);
		spnBanos.setValue(2);
		spnSalas.setValue(0);
		spnAparcamientos.setValue(0);
		listModel.clear();
		return true;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (txtLocalidad.getText().isEmpty()) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Introduzca la localidad", "Advertencia", JOptionPane.WARNING_MESSAGE);
		} else {
			// Convertir imágenes
			Vector<byte[]> imagenesByte = new Vector<byte[]>();
			for (int i = 0; i < listModel.size(); i++) {
				try {
					// Conversión de ImageIcon a BufferedImage
					ImageIcon icono = listModel.get(i);
					BufferedImage img = new BufferedImage(icono.getIconWidth(), icono.getIconHeight(), BufferedImage.TYPE_INT_RGB);
					Graphics g = img.createGraphics();
					icono.paintIcon(null, g, 0, 0);
					g.dispose();
					// Conversión de BufferedImage a byte[]
					ByteArrayOutputStream salidaBytes = new ByteArrayOutputStream();
					ImageIO.write(img, "jpg", salidaBytes);
					salidaBytes.flush();
					imagenesByte.add(salidaBytes.toByteArray());
					salidaBytes.close();
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error al cargar una imagen de la casa: " + e1.getMessage(), "Error", JOptionPane.WARNING_MESSAGE);
				}
			}
			// Crear el objeto casa
			try {
				MainWindow.getBusinessLogic().createRuralHouse(
						txtDescripcion.getText(), txtLocalidad.getText(), (Integer) spnHabitaciones.getValue(), (Integer) spnCocinas.getValue(),
						(Integer) spnBanos.getValue(), (Integer) spnSalas.getValue(), (Integer) spnAparcamientos.getValue(), imagenesByte, (Owner) MainWindow.getUser());
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Casa añadida con éxito", "Correcto", JOptionPane.INFORMATION_MESSAGE);
				MainWindow.getFrame().setCurrentPage(0);
			} catch (RemoteException e1) {
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
}
