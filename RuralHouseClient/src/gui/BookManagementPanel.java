package gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;

import domain.Book;
import domain.Owner;

public class BookManagementPanel extends ApplicationPage {

	private static final long serialVersionUID = 1L;

	private JCheckBoxMenuItem filtro[];
	private JList<Book> listBooks;
	private DefaultListModel<Book> listModel;
	private JScrollPane scrollBooks;
	
	private Vector<Book> reservas;

	public BookManagementPanel() {
		super("Gestionar reservas");
		
		// Crear barra del menú
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(50, 60, 700, 25);
		add(menuBar);

		// Construcción del primer menú
		JMenu menuFiltros = new JMenu("Ver");
		menuFiltros.setMnemonic(KeyEvent.VK_V);
		menuFiltros.getAccessibleContext().setAccessibleDescription("Filtros para las reservas mostradas en la lista");
		menuBar.add(menuFiltros);

		JMenuItem seleccionarTodos = new JMenuItem("Seleccionar todos");
		seleccionarTodos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				filtro[0].setSelected(true);
				filtro[1].setSelected(true);
				filtro[2].setSelected(true);
				filtro[3].setSelected(true);
				filtrar();
			}
		});
		seleccionarTodos.setMnemonic(KeyEvent.VK_S);
		menuFiltros.add(seleccionarTodos);
		
		menuFiltros.addSeparator();
		
		filtro = new JCheckBoxMenuItem[4];
		
		filtro[0] = new JCheckBoxMenuItem("No pagado");
		filtro[0].addActionListener(this);
		filtro[0].setMnemonic(KeyEvent.VK_N);
		menuFiltros.add(filtro[0]);

		filtro[1] = new JCheckBoxMenuItem("Sólo depósito pagado");
		filtro[1].addActionListener(this);
		filtro[1].setMnemonic(KeyEvent.VK_D);
		menuFiltros.add(filtro[1]);

		filtro[2] = new JCheckBoxMenuItem("A pagar en mano");
		filtro[2].addActionListener(this);
		filtro[2].setMnemonic(KeyEvent.VK_M);
		menuFiltros.add(filtro[2]);
		
		filtro[3] = new JCheckBoxMenuItem("Totalmente pagado");
		filtro[3].addActionListener(this);
		filtro[3].setMnemonic(KeyEvent.VK_T);
		menuFiltros.add(filtro[3]);

		// Construcción del segundo menú
		JMenu menuOperaciones = new JMenu("Operaciones");
		menuOperaciones.setMnemonic(KeyEvent.VK_O);
		menuOperaciones.getAccessibleContext().setAccessibleDescription("Operaciones disponibles para modificar una reserva");
		menuBar.add(menuOperaciones);
		
		JMenuItem opAnular = new JMenuItem("Anular reserva y poner oferta disponible");
		opAnular.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, ActionEvent.ALT_MASK));
		opAnular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				operar(1);
			}
		});
		menuOperaciones.add(opAnular);

		JMenuItem opDeposito = new JMenuItem("Marcar depósito pagado");
		opDeposito.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, ActionEvent.ALT_MASK));
		opDeposito.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				operar(2);
			}
		});
		menuOperaciones.add(opDeposito);

		JMenuItem opPagarEnMano = new JMenuItem("Marcar a pagar en mano");
		opPagarEnMano.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, ActionEvent.ALT_MASK));
		opPagarEnMano.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				operar(3);
			}
		});
		menuOperaciones.add(opPagarEnMano);

		JMenuItem opPagado = new JMenuItem("Marcar totalmente pagado");
		opPagado.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, ActionEvent.ALT_MASK));
		opPagado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				operar(4);
			}
		});
		menuOperaciones.add(opPagado);
		
		listModel = new DefaultListModel<Book>();
		listBooks = new JList<Book>(listModel);
		listBooks.setCellRenderer(new BookCellRenderer());
		listBooks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollBooks = new JScrollPane(listBooks);
		scrollBooks.setBounds(50, 85, 700, 480);
		add(scrollBooks);

		JButton btnVolver;
		try {
			// Cargar la imagen del botón
			BufferedImage iconoVolver = ImageIO.read(new File("res/button-back.png"));
			btnVolver = new JButton(new ImageIcon(iconoVolver));
			btnVolver.setBorder(BorderFactory.createEmptyBorder());
			btnVolver.setContentAreaFilled(false);
			btnVolver.setCursor(new Cursor(Cursor.HAND_CURSOR));
		} catch (IOException e) {
			System.err.println("Error al cargar la imagen: " + e.getMessage());
			// Añadir botón normal
			btnVolver = new JButton("Volver");
			btnVolver.setFont(new Font("Tahoma", Font.ITALIC, 14));
			btnVolver.setForeground(Color.BLUE);
		}
		btnVolver.setBounds(25, 5, 110, 43);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainWindow.getFrame().setCurrentPage(0);
			}
		});
		add(btnVolver);
	}

	@Override
	public boolean init() {
		try {
			// Obtiene las reservas del propietario
			reservas = MainWindow.getBusinessLogic().getBooks((Owner) MainWindow.getUser());
			if (reservas.isEmpty()) {
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "No hay ninguna reserva sobre ninguna de sus casas", "Advertencia", JOptionPane.WARNING_MESSAGE);
				return false;
			} else {
				filtro[0].setSelected(true);
				filtro[1].setSelected(true);
				filtro[2].setSelected(true);
				filtro[3].setSelected(true);
				filtrar();
				return true;
			}
		} catch (RemoteException e1) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}

	private void operar(int codigo) {
		if (listBooks.isSelectionEmpty()) {
			JOptionPane.showMessageDialog(MainWindow.getFrame(), "Seleccione una reserva antes de realizar una operación", "Advertencia", JOptionPane.WARNING_MESSAGE);
		} else {
			try {
				switch (codigo) {
				case 1:
					int respuesta = JOptionPane.showOptionDialog(MainWindow.getFrame(), 
							"<html>Está a punto de anular una reserva hecha por algún cliente. Esta acción no se puede deshacer.<br>" +
							"Si el cliente proporcionó un e-mail al reservar, será notificado de la cancelación.<br>" +
							"¿Ha entendido e igualmente desea continuar con la operación?</html>", "Advertencia", JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE, null, null, null);
					if (respuesta == JOptionPane.YES_OPTION) {
						// Eliminar reserva
						MainWindow.getBusinessLogic().deleteBook(listBooks.getSelectedValue());
						listModel.removeElementAt(listBooks.getSelectedIndex());
						JOptionPane.showMessageDialog(MainWindow.getFrame(), "Reserva anulada con éxito", "Correcto", JOptionPane.INFORMATION_MESSAGE);
					}
					break;
				case 2:
					// Marcar depósito pagado
					MainWindow.getBusinessLogic().updateBook(listBooks.getSelectedValue(), Book.DEPOSIT_PAID);
					listBooks.getSelectedValue().setStatus(Book.DEPOSIT_PAID);
					filtrar();
					break;
				case 3:
					// Marcar a pagar en mano
					MainWindow.getBusinessLogic().updateBook(listBooks.getSelectedValue(), Book.PAY_AT_CHECKIN);
					listBooks.getSelectedValue().setStatus(Book.PAY_AT_CHECKIN);
					filtrar();
					break;
				case 4:
					// Marcar totalmente pagado
					MainWindow.getBusinessLogic().updateBook(listBooks.getSelectedValue(), Book.TOTAL_PAID);
					listBooks.getSelectedValue().setStatus(Book.TOTAL_PAID);
					filtrar();
					break;
				}
			} catch (RemoteException e1) {
				JOptionPane.showMessageDialog(MainWindow.getFrame(), "Error inesperado al consultar el servidor remoto: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		filtrar();
	}
	
	// Rellena la lista con las reservas que cumplan los filtros
	private void filtrar() {
		listModel.clear();
		for (Book b : reservas) {
			if (filtro[b.getStatus()].isSelected())
				listModel.addElement(b);
		}
	}
	
}
