package domain;

import java.io.Serializable;

/**
 * Clase de usuario
 * No se instancia, en su lugar han de usarse sus clases hijas Owner y Client
 */
public abstract class User implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String email;
	private char[] password;
	private String phone;
	
	/**
	 * Default constructor
	 * @param email
	 * @param password
	 * @param phone
	 */
	public User(String email, char[] password, String phone) {
		this.email = email;
		this.password = password;
		this.phone = phone;
	}

	/**
	 * This method returns the user's e-mail
	 * @return An e-mail string
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * This method sets the user's e-mail
	 * @param email
	 *            new email address
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * This method returns the user's password
	 * @return An array of characters representing a password
	 */
	public char[] getPassword() {
		return password;
	}
	
	/**
	 * This method sets the user's password 
	 * @param password
	 *            new password
	 */
	public void setPassword(char[] password) {
		this.password = password;
	}
	
	/**
	 * This method returns the user's phone number
	 * @return A phone number string
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * This method sets the user's phone number
	 * @param phone
	 *            new phone number
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User)
			return (email.equals(((User) obj).email));
		return false;
	}
	
	@Override
	public String toString() {
		return email;
	}
}
