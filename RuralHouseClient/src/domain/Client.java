package domain;

import java.io.Serializable;
import java.util.Vector;

/**
 * Clase de cliente
 */
public class Client extends User implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Vector<Book> reservas;
	
	/**
	 * Default constructor
	 * @param email
	 * @param password
	 * @param telephone
	 */
	public Client(String email, char[] password, String telephone) {
		super(email, password, telephone);
		reservas = new Vector<Book>();
	}

	/**
	 * This method returns the client's booking list
	 * @return A vector of bookings
	 */
	public Vector<Book> getBookings() {
		return reservas;
	}
	
	/**
	 * This method adds the Book b to the booking list
	 * @param b - Booking to be added
	 */
	public void addBooking(Book b) {
		reservas.add(b);
	}
	
	/**
	 * This method deletes the Book b from the booking list, if it is contained
	 * @param b - Booking to be deleted
	 * @return true if the removal was successful
	 */
	public boolean removeBooking(Book b) {
		return reservas.remove(b);
	}
	
}
