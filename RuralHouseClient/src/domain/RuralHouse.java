package domain;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

/**
 * Clase de casa rural
 */
public class RuralHouse implements Comparable<RuralHouse>, Serializable {

	private static final long serialVersionUID = 1L;
	private static int lastHouseNumber = 0;

	private int houseNumber;
	private String description;
	private String city;
	private int nRooms, nKitchens, nBaths, nLivings, nParkings;
	private Vector<byte[]> pictures;
	private Vector<Comment> comments;
	private Owner owner;
	private Vector<Offer> offers;
	
	/**
	 * Default constructor
	 * @param description
	 * @param city
	 * @param rooms
	 * @param kitchens
	 * @param baths
	 * @param livings
	 * @param parkings
	 * @param owner
	 */
	public RuralHouse(String description, String city, int rooms, int kitchens, int baths, int livings, int parkings, Vector<byte[]> pictures, Owner owner) {
		this.houseNumber = ++lastHouseNumber;
		this.description = description;
		this.city = city;
		this.nRooms = rooms;
		this.nKitchens = kitchens;
		this.nBaths = baths;
		this.nLivings = livings;
		this.nParkings = parkings;
		this.pictures = pictures;
		comments = new Vector<Comment>();
		this.owner = owner;
		offers = new Vector<Offer>();
	}
	
	/**
	 * @return the houseNumber
	 */
	public int getHouseNumber() {
		return houseNumber;
	}

	/**
	 * @param houseNumber the houseNumber to set
	 */
	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the nRooms
	 */
	public int getRooms() {
		return nRooms;
	}

	/**
	 * @param nRooms the nRooms to set
	 */
	public void setRooms(int nRooms) {
		this.nRooms = nRooms;
	}

	/**
	 * @return the nKitchens
	 */
	public int getKitchens() {
		return nKitchens;
	}

	/**
	 * @param nKitchens the nKitchens to set
	 */
	public void setKitchens(int nKitchens) {
		this.nKitchens = nKitchens;
	}

	/**
	 * @return the nBaths
	 */
	public int getBaths() {
		return nBaths;
	}

	/**
	 * @param nBaths the nBaths to set
	 */
	public void setBaths(int nBaths) {
		this.nBaths = nBaths;
	}

	/**
	 * @return the nLivings
	 */
	public int getLivings() {
		return nLivings;
	}

	/**
	 * @param nLivings the nLivings to set
	 */
	public void setLivings(int nLivings) {
		this.nLivings = nLivings;
	}

	/**
	 * @return the nParkings
	 */
	public int getParkings() {
		return nParkings;
	}

	/**
	 * @param nParkings the nParkings to set
	 */
	public void setParkings(int nParkings) {
		this.nParkings = nParkings;
	}

	/**
	 * @return a vector of pictures
	 */
	public Vector<byte[]> getPictures() {
		return pictures;
	}
	
	/**
	 * @param pictures the vector of pictures to set
	 */
	public void setPictures(Vector<byte[]> pictures) {
		this.pictures = pictures;
	}

	/**
	 * This method adds a new comment about the house
	 * @param text - content of the comment
	 * @param rating - from 0.0 to a 5.0
	 * @return the created comment
	 */
	public Comment addComment(String text, float rating) {
		Comment com = new Comment(text, rating, this);
		comments.add(com);
		return com;
	}
	
	/**
	 * This method removes a comment from the list of the current house
	 * @param comment - comment to be deleted
	 * @return true if the object has been removed
	 */
	public boolean removeComment(Comment comment) {
		return comments.remove(comment);
	}
	
	/**
	 * @return a vector of comments
	 */
	public Vector<Comment> getComments() {
		return comments;
	}

	/**
	 * This methods returns a string with the average rating of the house
	 * @return a text string
	 */
	public String getAverageRating() {
		if (comments.isEmpty())
			return "No hay datos";
		float sumapuntuacion = 0.0f;
		for (Comment c : comments)
			sumapuntuacion += c.getRating();
		return String.format("%.1f sobre 5", sumapuntuacion / comments.size());
	}
	
	/**
	 * @return the owner
	 */
	public Owner getOwner() {
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	
	/**
	 * This method creates an offer for the current house with a first day, last day and price
	 * @param Offer start day, last day and price
	 * @return The created offer
	 */
	public Offer createOffer(Date firstDay, Date lastDay, float price) {
		Offer off = new Offer(this, firstDay, lastDay, price);
		offers.add(off);
		return off;
	}
	
	/**
	 * This method removes an offer from the list of the current house
	 * @param offer - the offer to be deleted
	 * @return true if the object has been removed
	 */
	public boolean removeOffer(Offer offer) {
		return offers.remove(offer);
	}

	/**
	 * This method obtains all the published offers for the house.
	 * @return a vector of offers
	 */
	public Vector<Offer> getOffers() {
		return offers;
	}
	
	/**
	 * This method obtains all the not past and not booked offers for the house.
	 * @return a vector of offers
	 */
	public Vector<Offer> getAvailableOffers() {
		Calendar today = Calendar.getInstance();
		today.set(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
		today.set(Calendar.MILLISECOND, 0);
		Vector<Offer> availableOffers = new Vector<Offer>();
		for (Offer off : offers) {
			if ((off.getFirstDay().compareTo(today.getTime()) >= 0) && (off.getBook() == null))
				availableOffers.add(off);
		}
		return availableOffers;
	}
	
	/**
	 * This method obtains offers whose dates conflict with the given offer.
	 * @param offer - the offer to check conflicts on
	 * @return a vector of offers conflicting with the given one
	 */
	public Vector<Offer> findConflictingOffers(Offer offer) {
		Vector<Offer> conflictingOffers = new Vector<Offer>();
		for (Offer o : offers) {
			if (!o.equals(offer) && (((o.getFirstDay().compareTo(offer.getFirstDay()) >= 0) && (o.getFirstDay().compareTo(offer.getLastDay()) <= 0)) ||
				((o.getLastDay().compareTo(offer.getFirstDay()) >= 0) && (o.getLastDay().compareTo(offer.getLastDay()) <= 0)) ||
				((o.getFirstDay().compareTo(offer.getFirstDay()) < 0) && (o.getLastDay().compareTo(offer.getLastDay()) > 0))))
					conflictingOffers.add(o);
		}
		return conflictingOffers;
	}
	
	/**
	 * This method obtains available offers for this concrete house in a certain date 
	 * @param firstDay, starting day of the offer
	 * @param lastDay, ending day of the offer
	 * @return a vector of offers available in these certain dates
	 */
	public Vector<Offer> findOffers(Date firstDay, Date lastDay) {
		Vector<Offer> availableOffers = new Vector<Offer>();
		for (Offer off : offers) {
			if (off.getFirstDay().equals(firstDay) && off.getLastDay().equals(lastDay) && (off.getBook() == null))
				availableOffers.add(off);
		}
		return availableOffers;
	}
	
	@Override
	public int compareTo(RuralHouse rh) {
		return Integer.compare(houseNumber, rh.houseNumber);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RuralHouse)
			return (houseNumber == ((RuralHouse) obj).houseNumber);
		return false;
	}
	
	@Override
	public String toString() {
		return "Casa número " + houseNumber + " en " + city + ", " + offers.size() + " oferta(s)";
	}

	/**
	 * @return the lastHouseNumber
	 */
	public static int getLastHouseNumber() {
		return lastHouseNumber;
	}

	/**
	 * @param lastHouseNumber - the lastHouseNumber to set
	 */
	public static void setLastHouseNumber(int lastHouseNumber) {
		RuralHouse.lastHouseNumber = lastHouseNumber;
	}
	
}
