package domain;

import java.io.Serializable;
import java.util.Date;

import configuration.Config;

/**
 * Clase de comentarios y valoración
 */
public class Comment implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Date date;
	private String text;
	private float rating;
	private RuralHouse ruralHouse;
	
	/**
	 * Default constructor
	 * @param text
	 * @param rating
	 * @param ruralHouse
	 */
	public Comment(String text, float rating, RuralHouse ruralHouse) {
		this.date = new Date();
		this.text = text;
		this.rating = rating;
		this.ruralHouse = ruralHouse;
	}

	/**
	 * Returns the date the comment was made
	 * @return a date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Sets the date the comment was made
	 * @param date
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Returns the text of the comment
	 * @return a text string
	 */
	public String getText() {
		return text;
	}

	/**
	 * Sets the text of the comment
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Returns the rating of the comment
	 * @return a float number between 0.0 and 5.0
	 */
	public float getRating() {
		return rating;
	}

	/**
	 * Sets the rating of the comment
	 * @param rating
	 */
	public void setRating(float rating) {
		this.rating = rating;
	}

	/**
	 * Returns the rural house which is commented
	 * @return the RuralHouse
	 */
	public RuralHouse getRuralHouse() {
		return ruralHouse;
	}

	/**
	 * Sets the rural house which is commented
	 * @param ruralHouse
	 */
	public void setRuralHouse(RuralHouse ruralHouse) {
		this.ruralHouse = ruralHouse;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Comment)
			return date.equals(((Comment) obj).getDate()) && text.equals(((Comment) obj).getText())
					&& (rating == ((Comment) obj).getRating()) && ruralHouse.equals(((Comment) obj).getRuralHouse());
		return false;
	}

	@Override
	public String toString() {
		return "Comentado el " + Config.getShortDateFormat().format(date) + ": " + text;
	}

}
