package configuration;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Locale;

public final class Config {

	private final static String serverRMI = "localhost";
	private final static int portRMI = 50099;
	private final static String serviceRMI = "RuralHouses";
	private final static String javaPolicyPath = "java.policy";

	private final static boolean openExistingDatabase = true;
	private final static String db4oFilename = "casas.db4o";
	
	private final static Locale locale = new Locale("es", "ES");
	private final static DecimalFormatSymbols simbolosFormatoPrecio = new DecimalFormatSymbols(locale);
	private final static DecimalFormat formatoPrecio = new DecimalFormat("#0.00", simbolosFormatoPrecio);
	private final static SimpleDateFormat formatoFechaCompleta = new SimpleDateFormat("EEEE, d 'de' MMMM 'del' y", locale);
	private final static SimpleDateFormat formatoFechaCorta = new SimpleDateFormat("dd/MM/yyyy", locale);

	private Config() {
	}

	public static String getServerRMI() {
		return serverRMI;
	}

	public static int getPortRMI() {
		return portRMI;
	}

	public static String getServiceRMI() {
		return serviceRMI;
	}

	public static String getJavaPolicyPath() {
		return javaPolicyPath;
	}

	public static String getDb4oFilename() {
		return db4oFilename;
	}

	public static boolean getDataBaseOpenMode() {
		return openExistingDatabase;
	}
	
	public static Locale getLocale() {
		return locale;
	}

	public static DecimalFormat getDecimalFormat() {
		return formatoPrecio;
	}
	
	public static void setDecimalSeparator() {
		simbolosFormatoPrecio.setDecimalSeparator(',');
	}
	
	public static SimpleDateFormat getFullDateFormat() {
		return formatoFechaCompleta;
	}
	
	public static SimpleDateFormat getShortDateFormat() {
		return formatoFechaCorta;
	}
	
}
