# Casas rurales #
## Proyecto de Ingenier�a del Software I ##

### Autores: Iv�n Matellanes y Luis Javier Salvatierra ###

### Prerrequisitos a instalar ###
+ EGit: http://download.eclipse.org/egit/updates

### Configuraci�n de EGit ###
En Window > Preferences > Team > Git > Configuration > Pesta�a "User settings", a�adir estas 2 entradas:

1. user.name --> Nombre Apellido
2. user.email --> E-mail con el que te has registrado en Bitbucket

Guardar los cambios clickando "Apply"

### Configuraci�n SSH ###
En Eclipse, ir a Window > Preferences > General > Network Connections > SSH2, pesta�a "Key Management".  
Pulsar en "Generate RSA Key" y poner una contrase�a en Passphrase.  
*Esta contrase�a se te pedir� cada vez que quieras acceder al repositorio*  
Guardar la clave en la carpeta por defecto clickando "Save Private Key".  
Copiar la clave que aparece en la caja de texto, y pegarla en tu cuenta (ir en la web de Bitbucket a Manage account > SSH keys) pulsando "Add Key".  
*De esta forma, tu ordenador queda asociado con la cuenta de Bitbucket por SSH*  

### Configuraci�n del proyecto ###
Importar el proyecto a Eclipse desde Git (File > Import > Git > URI - Direcci�n que aparece en la p�gina del repositorio en Bitbucket).     

### Trabajar con los repositorios ###
Cada uno de nosotros accede a 2 repositorios: local (en nuestro ordenador, cada uno el suyo) y remoto (en Bitbucket, com�n para los dos).  
Las acciones que vamos a usar son las siguientes:

+ **Add to index**: Cuando quer�is a�adir un archivo nuevo al repositorio, lo primero es a�adirlo al �ndice.
+ **Commit**: Es guardar los cambios que has hecho en el repositorio local. Hay que escribir una descripci�n de lo que se ha hecho. No conviene hacerlo cada 5 minutos, sino cuando terminas de hacer algo. P. ej.: "He a�adido el caso de uso Reservar"
+ **Push**: Subir los cambios que has hecho en el repositorio local al remoto, de modo que los dem�s podamos recibirlos. Hay que asegurarse de que tienes el local actualizado respecto al remoto, de lo contrario dar� `error: non fast forward` y primero habr� que hacer *pull*.
+ **Pull**: Bajarse los �ltimos cambios que hayan hecho los dem�s en el repositorio remoto al local.

Todas ellas se realizan desde Eclipse haciendo click con el bot�n derecho en la carpeta del Project Explorer > Team.  